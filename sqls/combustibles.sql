--
-- Table structure for table `combustibles`
--

CREATE TABLE `combustibles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `combustibles`
--
ALTER TABLE `combustibles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `combustibles`
--
ALTER TABLE `combustibles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;