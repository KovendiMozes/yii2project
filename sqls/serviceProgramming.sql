--
-- Table structure for table `serviceProgramming`
--

CREATE TABLE `serviceProgramming` (
  `id` int(11) NOT NULL,
  `position` varchar(15) NOT NULL,
  `workerId` int(11) NOT NULL,
  `workTypeId` int(11) NOT NULL,
  `overlapDays` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `serviceProgramming`
--
ALTER TABLE `serviceProgramming`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `serviceProgramming`
--
ALTER TABLE `serviceProgramming`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;