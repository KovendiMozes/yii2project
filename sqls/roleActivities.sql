--
-- Table structure for table `roleActivities`
--

CREATE TABLE `roleActivities` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `activityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `roleActivities`
--
ALTER TABLE `roleActivities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `roleActivities`
--
ALTER TABLE `roleActivities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;