--
-- Table structure for table `carCategories`
--

CREATE TABLE `carCategories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `carCategories`
--
ALTER TABLE `carCategories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `carCategories`
--
ALTER TABLE `carCategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;