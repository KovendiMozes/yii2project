--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `activities` (`id`, `name`, `description`) VALUES
(1, 'brands/index', ''),
(2, 'brands/create', ''),
(3, 'brands/view', ''),
(4, 'brands/update', ''),
(5, 'users/index', ''),
(6, 'users/update', ''),
(7, 'roles/index', ''),
(8, 'roles/update', ''),
(11, 'bodyworks/index', ''),
(12, 'bodyworks/create', ''),
(13, 'bodyworks/update', ''),
(14, 'bodyworks/view', ''),
(16, 'carcategories/index', ''),
(17, 'carcategories/create', ''),
(18, 'carcategories/update', ''),
(19, 'carcategories/view', ''),
(21, 'caremissions/index', ''),
(22, 'caremissions/create', ''),
(23, 'caremissions/update', ''),
(24, 'caremissions/view', ''),
(26, 'carseatnumbers/index', ''),
(27, 'carseatnumbers/create', ''),
(28, 'carseatnumbers/update', ''),
(29, 'carseatnumbers/view', ''),
(31, 'combustibles/index', ''),
(32, 'combustibles/create', ''),
(33, 'combustibles/update', ''),
(34, 'combustibles/view', ''),
(36, 'driventypes/index', ''),
(37, 'driventypes/create', ''),
(38, 'driventypes/update', ''),
(39, 'driventypes/view', ''),
(41, 'equipmentlevels/index', ''),
(42, 'equipmentlevels/create', ''),
(43, 'equipmentlevels/update', ''),
(44, 'equipmentlevels/view', ''),
(46, 'gearboxes/index', ''),
(47, 'gearboxes/create', ''),
(48, 'gearboxes/update', ''),
(49, 'gearboxes/view', ''),
(51, 'languages/index', ''),
(52, 'languages/create', ''),
(53, 'languages/update', ''),
(54, 'languages/view', ''),
(56, 'statuses/index', ''),
(57, 'statuses/create', ''),
(58, 'statuses/update', ''),
(59, 'statuses/view', ''),
(61, 'translations/index', ''),
(62, 'translations/create', ''),
(63, 'translations/update', ''),
(64, 'translations/view', ''),
(66, 'roles/create', ''),
(67, 'roles/view', ''),
(69, 'users/create', ''),
(70, 'users/view', ''),
(72, 'brandtype/index', ''),
(73, 'brandtype/create', '');