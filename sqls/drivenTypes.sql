--
-- Table structure for table `drivenTypes`
--

CREATE TABLE `drivenTypes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `drivenTypes`
--
ALTER TABLE `drivenTypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `drivenTypes`
--
ALTER TABLE `drivenTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;