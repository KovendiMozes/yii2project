--
-- Table structure for table `bodyworks`
--

CREATE TABLE `bodyworks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `bodyworks`
--
ALTER TABLE `bodyworks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `bodyworks`
--
ALTER TABLE `bodyworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;