--
-- Table structure for table `workTypes`
--

CREATE TABLE `workTypes` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `workTypes`
--
ALTER TABLE `workTypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `workTypes`
--
ALTER TABLE `workTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
