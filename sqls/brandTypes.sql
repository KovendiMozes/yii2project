--
-- Table structure for table `brandTypes`
--

CREATE TABLE `brandTypes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `brandId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `brandTypes`
--
ALTER TABLE `brandTypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `brandTypes`
--
ALTER TABLE `brandTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;