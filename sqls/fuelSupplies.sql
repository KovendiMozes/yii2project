--
-- Table structure for table `fuelSupplies`
--

CREATE TABLE `fuelSupplies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `fuelSupplies`
--
ALTER TABLE `fuelSupplies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `fuelSupplies`
--
ALTER TABLE `fuelSupplies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;