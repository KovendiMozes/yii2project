--
-- Table structure for table `gearboxes`
--

CREATE TABLE `gearboxes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `gearboxes`
--
ALTER TABLE `gearboxes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `gearboxes`
--
ALTER TABLE `gearboxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;