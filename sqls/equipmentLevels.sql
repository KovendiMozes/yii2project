--
-- Table structure for table `equipmentLevels`
--

CREATE TABLE `equipmentLevels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `equipmentLevels`
--
ALTER TABLE `equipmentLevels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `equipmentLevels`
--
ALTER TABLE `equipmentLevels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;