--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `brandTypeId` int(11) NOT NULL,
  `typeVersionId` int(11) NOT NULL,
  `carCategorieId` int(11) NOT NULL,
  `bodyworkId` int(11) NOT NULL,
  `carColorId` int(11) NOT NULL,
  `carEmissionId` int(11) NOT NULL,
  `carSeatNumberId` int(11) NOT NULL,
  `combustibleId` int(11) NOT NULL,
  `drivenTypeId` int(11) NOT NULL,
  `equipmentLevelId` int(11) NOT NULL,
  `gearboxId` int(11) NOT NULL,
  `statusId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `fuelSupplyId` int(11) NOT NULL,
  `pedigreeNumber` varchar(7) NOT NULL,
  `mileage` int(11) NOT NULL,
  `vintage` date NOT NULL,
  `firstEntryDate` date NOT NULL,
  `bodyNumber` varchar(17) NOT NULL,
  `licensePlate` varchar(9) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;