--
-- Table structure for table `carSeatNumbers`
--

CREATE TABLE `carSeatNumbers` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `carSeatNumbers`
--
ALTER TABLE `carSeatNumbers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `carSeatNumbers`
--
ALTER TABLE `carSeatNumbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;