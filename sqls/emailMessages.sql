--
-- Table structure for table `emailMessages`
--

CREATE TABLE `emailMessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `createdAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `emailMessages`
--
ALTER TABLE `emailMessages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `emailMessages`
--
ALTER TABLE `emailMessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;