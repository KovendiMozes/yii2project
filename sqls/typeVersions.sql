--
-- Table structure for table `typeVersions`
--

CREATE TABLE `typeVersions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `brandTypeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `typeVersions`
--
ALTER TABLE `typeVersions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `typeVersions`
--
ALTER TABLE `typeVersions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;