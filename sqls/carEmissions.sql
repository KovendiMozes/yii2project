--
-- Table structure for table `carEmissions`
--

CREATE TABLE `carEmissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `carEmissions`
--
ALTER TABLE `carEmissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `carEmissions`
--
ALTER TABLE `carEmissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;