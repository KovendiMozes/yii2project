--
-- Table structure for table `carColors`
--

CREATE TABLE `carColors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `carColors`
--
ALTER TABLE `carColors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `carColors`
--
ALTER TABLE `carColors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;