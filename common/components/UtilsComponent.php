<?php 
namespace common\components;

use Yii;
use yii\helpers\Url;
use yii\base\Component;

class UtilsComponent extends Component
{

    public function init()
    {
        parent::init();
    }

    public function createAbsoluteUrl($params = ['/']){
        return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].Url::to($params);
    }

    public function debugger($data){
        echo '<pre>'; print_r($data); echo '</pre>';
    }
}

?>