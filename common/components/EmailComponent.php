<?php 
namespace common\components;

use Yii;
use yii\base\Component;

class EmailComponent extends Component
{

    public function init()
    {
        parent::init();
    }

    public function create($emailMessage, $user) {
        $user = $user;
        $emailMessage = $emailMessage;
        $emailMessage->userId = Yii::$app->user->id;
        if($emailMessage->save()){
            $this->send($emailMessage, $user);
        }
    }

    private function send($emailMessage, $user){
        return Yii::$app
            ->mailer
            ->compose()
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject($emailMessage->subject)
            ->setTextBody($emailMessage->content)
            ->send();
    }
}
?>