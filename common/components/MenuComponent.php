<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

class MenuComponent extends Component
{

    public function init()
    {
        parent::init();
    }

    private function createCategories()
    {
        $list = [
            'carBaseParameters' => [
                'icon' => 'fas fa-clipboard-list',
                'items' => [],
            ],
            'carOtherParameters' => [
                'icon' => 'fas fa-clipboard-list',
                'items' => [],
            ],
            'users' => [
                'icon' => 'fas fa-user',
                'items' => [],
            ],
            'other' => [
                'icon' => 'fas fa-cog',
                'items' => [],
            ],
            'service' => [
                'icon' => 'fas fa-tools',
                'items' => [],
            ],
            'cars' => [
                'icon' => 'fas fa-car',
                'items' => [],
            ]
        ];

        return $list;
    }

    function list() {

        $list = $this->createCategories();

        $list['users']['items'] = [
            'users' => [
                'label' => Yii::t('app', 'users'),
                'url' => Url::to(['/users']),
            ],
            'createUser' => [
                'label' => Yii::t('app', 'createUser'),
                'url' => Url::to(['/users/create']),
            ],
            'roles' => [
                'label' => Yii::t('app', 'roles'),
                'url' => Url::to(['/roles']),
            ],
        ];

        $list['carBaseParameters']['items'] = [
            'brand' => [
                'label' => Yii::t('app', 'brands'),
                'url' => Url::to(['/brands']),
            ],
            'createBrand' => [
                'label' => Yii::t('app', 'createBrand'),
                'url' => Url::to(['/brands/create']),
            ],
            'brandTypes' => [
                'label' => Yii::t('app', 'brandTypes'),
                'url' => Url::to(['/brandtype']),
            ],
            'createBrandType' => [
                'label' => Yii::t('app', 'createBrandType'),
                'url' => Url::to(['/brandtype/create']),
            ],
            'typeVersions' => [
                'label' => Yii::t('app', 'typeVersions'),
                'url' => Url::to(['/typeversions']),
            ],
            'createTypeVersions' => [
                'label' => Yii::t('app', 'createTypeVersions'),
                'url' => Url::to(['/typeversions/create']),
            ],
        ];

        $list['carOtherParameters']['items'] = [
            'bodyworks' => [
                'label' => Yii::t('app', 'bodyworks'),
                'url' => Url::to(['/bodyworks']),
            ],
            'carcolors' => [
                'label' => Yii::t('app', 'carColors'),
                'url' => Url::to(['/carcolors']),
            ],
            'carcategories' => [
                'label' => Yii::t('app', 'carCategories'),
                'url' => Url::to(['/carcategories']),
            ],
            'caremissions' => [
                'label' => Yii::t('app', 'carEmissions'),
                'url' => Url::to(['/caremissions']),
            ],
            'carseatnumbers' => [
                'label' => Yii::t('app', 'carSeatNumbers'),
                'url' => Url::to(['/carseatnumbers']),
            ],
            'combustibles' => [
                'label' => Yii::t('app', 'combustibles'),
                'url' => Url::to(['/combustibles']),
            ],
            'driventypes' => [
                'label' => Yii::t('app', 'drivenTypes'),
                'url' => Url::to(['/driventypes']),
            ],
            'equipmentlevels' => [
                'label' => Yii::t('app', 'equipmentLevels'),
                'url' => Url::to(['/equipmentlevels']),
            ],
            'gearboxes' => [
                'label' => Yii::t('app', 'gearboxes'),
                'url' => Url::to(['/gearboxes']),
            ],
            'statuses' => [
                'label' => Yii::t('app', 'statuses'),
                'url' => Url::to(['/statuses']),
            ],
            'fuelSupplies' => [
                'label' => Yii::t('app', 'fuelSupplies'),
                'url' => Url::to(['/fuelsupplies']),
            ],
            'engines' => [
                'label' => Yii::t('app', 'engines'),
                'url' => Url::to(['/engines']),
            ],
        ];

        $list['other']['items'] = [
            'translations' => [
                'label' => Yii::t('app', 'translations'),
                'url' => Url::to(['/translations']),
            ],
            'logs' => [
                'label' => Yii::t('app', 'logs'),
                'url' => Url::to(['/logs']),
            ],
        ];

        $list['service']['items'] = [
            'serviceprogrammings' => [
                'label' => Yii::t('app', 'serviceProgramming'),
                'url' => Url::to(['/serviceprogramming']),
            ],
            'worktypes' => [
                'label' => Yii::t('app', 'workTypes'),
                'url' => Url::to(['/worktypes']),
            ],
            'workers' => [
                'label' => Yii::t('app', 'workers'),
                'url' => Url::to(['/workers']),
            ],
        ];

        $list['cars']['items'] = [
            'cars' => [
                'label' => Yii::t('app', 'cars'),
                'url' => Url::to(['/cars']),
            ],
            'createCar' => [
                'label' => Yii::t('app', 'createCar'),
                'url' => Url::to(['/cars/create']),
            ],
        ];

        return $this->availableMenus($list);
    }

    private function availableMenus($list)
    {
        $menuPanels = [];
        foreach ($list as $categoryIndex => $category) {
            $menuPanels[$categoryIndex] = [
                'icon' => $category['icon'],
                'items' => [],
            ];
            $removePanel = true;
            foreach ($category['items'] as $itemIndex => $item) {
                if (Yii::$app->protect->isEnabled($item['url'])) {
                    $menuPanels[$categoryIndex]['items'][$itemIndex] = $item;
                    $removePanel = false;
                }
            }
            if ($removePanel) {
                unset($menuPanels[$categoryIndex]);
            }
        }

        $listWithColumns = [
            'aColumn' => [],
            'bColumn' => [],
            'cColumn' => [],
        ];

        $previewColumns = [];
        foreach ($menuPanels as $label => $menuPanel) {
            if (!in_array('aColumn', $previewColumns)) {
                $previewColumns[] = 'aColumn';
                $listWithColumns['aColumn'][$label] = $menuPanel;
                continue;
            }
            if (!in_array('bColumn', $previewColumns)) {
                $previewColumns[] = 'bColumn';
                $listWithColumns['bColumn'][$label] = $menuPanel;
                continue;
            }
            $previewColumns = [];
            $listWithColumns['cColumn'][$label] = $menuPanel;
        }

        return $listWithColumns;
    }
}
