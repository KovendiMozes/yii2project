<?php
namespace common\components;

use backend\models\UserRole;
use Yii;
use yii\base\Component;
use yii\web\Cookie;

class ProtectComponent extends Component
{
    public function init()
    {
        parent::init();
    }

    private function getActionFromUrl($url)
    {
        if (strpos($url, '?') !== false) {
            $url = explode('?', $url)[0];
        }
        if (strpos($url, '/backend/') !== false) {
            $url = str_replace('/backend/', '', $url);
        }
        return $url;
    }

    private function getActivities()
    {
        $activities = [];
        $userRoles = UserRole::find()->where(['userId' => Yii::$app->user->id])->all();
        if (!$userRoles) {
            return $result;
        }
        foreach ($userRoles as $role) {
            foreach ($role->roleActivity as $ra) {
                if (!in_array($ra->activity->name, $activities)) {
                    $activities[] = $ra->activity->name;
                    if (strpos($ra->activity->name, '/index') !== false) {
                        $activities[] = explode('/', $ra->activity->name)[0];
                    }
                }
            }
        }
        return $activities;
    }

    public function set()
    {
        $cookies = Yii::$app->response->cookies;
        $cookie = new Cookie([
            'name' => 'activities',
            'value' => $this->getActivities()]);
        $cookies->add($cookie);
    }

    public function isEnabled($url)
    {
        $action = $this->getActionFromUrl($url);
        $cookies = Yii::$app->response->cookies;
        if (in_array($action, $cookies->get('activities')->value)) {
            return true;
        }
        return false;
    }
}
