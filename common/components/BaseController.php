<?php 
namespace common\components;

use Yii;
use yii\web\Controller;

class BaseController extends Controller{

    public function init(){
        parent::init();
        if(!Yii::$app->user->isGuest){
            Yii::$app->protect->set();
        }
    }

}

?>