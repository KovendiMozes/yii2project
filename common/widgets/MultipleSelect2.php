<?php

namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


class MultipleSelect2 extends Widget{

    public $model;
    public $form;
    public $attribute;
    public $data;

    public function init(){
        parent::init();
    }

    private function buildData()
    {
        $data = ArrayHelper::map($this->data, 'id', 'name');
        $row = [];
        foreach($data as $d){
            $row[$d]=$d;
        }

        return $row;
    }

    public function run(){
        return $this->render('@common/widgets/multipleselect2/view',[
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'data' => $this->buildData()
        ]);
    }
}
