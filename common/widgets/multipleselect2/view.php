<?php
use kartik\select2\Select2;
?>
<?= $form->field($model, $attribute)->widget(Select2::classname(), [
    'data' => $data,
    'language' => 'en',
    'name' => $attribute.'[]',
    'options' => ['placeholder' => $model->getAttributeLabel($attribute)],
    'pluginOptions' => [
        'tags' => true,
        'allowClear' => true,
        'multiple' => true
    ],
])->label($model->getAttributeLabel($attribute)); 
?>