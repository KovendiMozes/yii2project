<?php
namespace common\widgets;

use yii\base\Widget;

class Select2 extends Widget
{
    public $url;
    public $model;
    public $form;
    public $attribute;
    public $depend;
    public $staticValues;
    public $textTranslate;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('@common/widgets/select2/view', [
            'url' => $this->url,
            'model' => $this->model,
            'form' => $this->form,
            'attribute' => $this->attribute,
            'depend' => $this->depend,
            'staticValues' => $this->staticValues,
            'textTranslate' => $this->textTranslate,
        ]);
    }
}
