<?php

namespace common\widgets;

use yii\base\Widget;

class DatePicker extends Widget
{
    public $form;
    public $model;
    public $attribute;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('@common/widgets/datepicker/view', [
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
        ]);
    }
}
