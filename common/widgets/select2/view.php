<?php
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

$valueFromGet = $model->$attribute;
$classPath = get_class($model);
$exploded = explode("\\", $classPath);
$className = $exploded[count($exploded) - 1];
if (isset($_GET[$className][$attribute]) && $_GET[$className][$attribute]) {
    $valueFromGet = $_GET[$className][$attribute];
}

$model->$attribute = $valueFromGet;
if ($depend) {
    $field = $form->field($model, $attribute)->widget(DepDrop::classname(), [
        'data' => [],
        'options' => ['placeholder' => Yii::t('app', 'loading') . '...'],
        'type' => DepDrop::TYPE_SELECT2,
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'pluginOptions' => [
            'depends' => $depend,
            'initialize' => $model->isNewRecord ? false : true,
            'url' => Url::to($url),
            'loadingText' => Yii::t('app', 'loading') . '...',
        ],
    ]);
    $field->enableClientValidation = false;
    echo $field;
} elseif (!$depend && !$staticValues) {
    echo $form->field($model, $attribute)->widget(Select2::className(), [
        'options' => ['id', 'placeholder' => Yii::t('app', 'search') . '...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'language' => [
                'errorLoading' => new JsExpression("function () { return '" . Yii::t('app', 'Error Loading') . "'; }"),
            ],
            'ajax' => [
                'url' => Url::to($url),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {value:params.term}; }'),
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        ],
    ]);
} elseif ($staticValues) {
    $values = [];
    foreach ($staticValues as $value) {
        $values[$value['id']] = $textTranslate ? Yii::t('app', $value[$attribute]) : $value[$attribute];
    }
    echo $form->field($model, $attribute)->widget(Select2::className(), [
        'data' => $values,
        'options' => ['placeholder' => $model->getAttributeLabel($attribute), 'value' => $valueFromGet],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
} else {
    echo 'Invalid select.';
}
