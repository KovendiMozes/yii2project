<?php
use kartik\date\DatePicker;

echo $form->field($model, $attribute)->widget(DatePicker::className(), [
    'name' => $attribute,
    'options' => ['placeholder' => $model->getAttributeLabel($attribute), 'autocomplete' => 'off'],
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
    ],
]);
