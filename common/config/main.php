<?php
// use kartik\mpdf\Pdf;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'name' => 'Vo3admin',
    'language' => 'hu',
    'timeZone' => 'Europe/Bucharest',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'email' => [
            'class' => 'common\components\EmailComponent'
        ],
        'utils' => [
            'class' => 'common\components\UtilsComponent'
        ],
        'menu' => [
            'class' => 'common\components\MenuComponent'
        ],
        'protect' => [
            'class' => 'common\components\ProtectComponent'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/translate',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        // 'pdf' => [
        //     'class' => Pdf::classname(),
        //     'format' => Pdf::FORMAT_A4,
        //     'orientation' => Pdf::ORIENT_PORTRAIT,
        //     'destination' => Pdf::DEST_BROWSER,
        //     // refer settings section for all configuration options
        // ]
    ],
];
