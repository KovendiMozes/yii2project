<?php
return [
    'adminEmail' => 'vo3admin-support@skytransauto.ro',
    'supportEmail' => 'vo3admin-support@skytransauto.ro',
    'senderEmail' => 'vo3admin-support@skytransauto.ro',
    'senderName' => 'vo3admin',
    'user.passwordResetTokenExpire' => 3600,
    'poweredBy' => 'Powered by Kövendi brothers'
];
