<?php
namespace common\models;

use Yii;
use yii\base\Model;


/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['email', 'emailValidation']
        ];
    }

    public function emailValidation() {
        if($this->email && (strpos($this->email, '@') === false || strpos($this->email, '.') === false)) {
            return $this->addError('email', Yii::t('app', 'invalidEmailMessage', ['email' => $this->email]).'.');
        }
    }

    public function attributeLabels(){
        return [
            'email' => 'Email',
            'password' => Yii::t('app', 'password'),
            'rememberMe' => Yii::t('app', 'rememberMe')
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional email-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate() && $user) {
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()->where([
                'email' => $this->email
            ])->one();
        }

        if($this->_user !== null && $this->_user->password_hash == null){
            return false;
        }

        return $this->_user;
    }
}
