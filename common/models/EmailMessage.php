<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "emailMessages".
 *
 * @property int $id
 * @property int $userId
 * @property string $subject
 * @property string $content
 * @property string $createdAt
 * @property string $updatedAt
 */
class EmailMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emailMessages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'subject', 'content'], 'required'],
            [['userId'], 'integer'],
            [['content'], 'string'],
            [['createdAt'], 'safe'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'subject' => 'Subject',
            'content' => 'Content',
            'createdAt' => 'Created At',
        ];
    }

    public function beforeSave($insert){
        $this->createdAt = date('Y-m-d H:i:s');
        return parent::beforeSave($insert);
    }
}
