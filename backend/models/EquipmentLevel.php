<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "equipmentLevels".
 *
 * @property int $id
 * @property string $name
 * @property int $userId
 * @property int $brandId
 * @property string $createdAt
 * @property string $updatedAt
 */
class EquipmentLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipmentLevels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['userId'], 'integer'],
            [['createdAt', 'updatedAt', 'userId', 'brandId'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['name', 'uniqueName']
        ];
    }

    public function uniqueName(){

        $model = EquipmentLevel::find()->where(['name'=>$this->name])->one();

        if($model){
            return $this->addError('name', Yii::t('app','equipmentLevelUniqueNameError', [
                'name'=>$this->name
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'equipmentLevelName'),
            'userId' => Yii::t('app', 'userName'),
            'brandId'=> Yii::t('app', 'brand'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->brandId = Brand::find()->where(['name' => $this->brandId])->one()->id;
        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'userId']);
    }
    
    public function getBrand(){
        return $this->hasOne(Brand::className(),['id' => 'brandId']);
    }

    public static function getEquipmentLevelListByBrandName($brandName){
        $brandIdSearch = Brand::find()->select(['id' => 'id'])->where(['name' => $brandName])->asArray()->all();
        $equipmentLevels = self::find()->select(['id' => 'id','name' => 'name'])->where(['brandId' => $brandIdSearch[0]["id"]])->asArray()->all();
        return $equipmentLevels;
    }
}
