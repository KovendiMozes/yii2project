<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['user_id', 'integer'],
            [['user_id', 'created_at', 'updated_at'], 'safe'],
            ['name', 'string', 'max' => 255],
            ['name', 'uniqueName'],
        ];
    }

    public function uniqueName(){

        $model = Brand::find()->where(['name'=>$this->name])->one();

        if($model){
            return $this->addError('name', Yii::t('app','uniqueNameError', [
                'brand'=>$this->name
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app','appellation'),
            'user_id' => Yii::t('app','userName'),
            'created_at' => Yii::t('app','createdAt'),
            'updated_at' => Yii::t('app','updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->user_id = Yii::$app->user->id;
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = $this->created_at;

        return parent::beforeSave($insert);

    }

    public  function getUser(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }
}
