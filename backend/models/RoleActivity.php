<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "roleActivities".
 *
 * @property int $id
 * @property int $roleId
 * @property int $activityId
 */
class RoleActivity extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'roleActivities';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['roleId', 'activityId'], 'required'],
			[['roleId', 'activityId'], 'integer'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'roleId' => 'Role ID',
			'activityId' => 'Activity ID',
		];
	}


	public function getActivity() {
		return $this->hasOne(Activity::className(), ['id' => 'activityId']);
	}

}
