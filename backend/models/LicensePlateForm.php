<?php
namespace backend\models;

use Yii;
use yii\base\Model;


/**
 * LicensePlateForm form
 */
class LicensePlateForm extends Model
{
    public $county;
    public $twoDigitNumber;
    public $threeLetters;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['county', 'twoDigitNumber', 'threeLetters'], 'required'],
            ['county', 'string', 'min' => 1, 'max' => 2],
            ['twoDigitNumber', 'string', 'min' => 2, 'max' => 3],
            ['threeLetters', 'string', 'min' => 3, 'max' => 3],
            [['county', 'threeLetters'], 'doesNotContainNumber'],
            ['twoDigitNumber', 'isNumeric'],
            [['county', 'twoDigitNumber'], 'exceptionCounty'],
        ];
    }

    public function exceptionCounty(){
        if(strcmp($this->county, _Constants::SHORT_NAME_BUCAREST) == 0){
            if(intval($this->twoDigitNumber) > 999){
                return $this->addError('twoDigitNumber', Yii::t('app', 'twoDigitNumberBucarestError', [
                    'twoDigitNumber' => $this->twoDigitNumber
                ]));
            }
        }else{
            if(intval($this->twoDigitNumber) > 99){
                return $this->addError('twoDigitNumber', Yii::t('app', 'twoDigitNumberMaxError', [
                    'twoDigitNumber' => $this->twoDigitNumber
                ]));
            }
        }
    }

    public function isNumeric(){
        if(!is_numeric($this->twoDigitNumber)){
            return $this->addError('twoDigitNumber', Yii::t('app', 'twoDigitNumberNumericError', [
                'twoDigitNumber' => $this->twoDigitNumber
            ]));
        }
    }

    public function doesNotContainNumber(){
        if(preg_match('~[0-9]+~', $this->county)) {
            $this->addError('county', Yii::t('app', 'countyError', [
                'county' => $this->county
            ]));
        }

        if(preg_match('~[0-9]+~', $this->threeLetters)) {
            $this->addError('threeLetters', Yii::t('app', 'threeLettersError', [
                'threeLetters' => $this->threeLetters
            ]));
        }

        return $this->getErrors();
    }

    public function attributeLabels(){
        return [
            'county' => Yii::t('app', 'county'),
            'twoDigitNumber' => Yii::t('app', 'twoDigitNumber'),
            'threeLetters' => Yii::t('app', 'threeLetters'),
        ];
    }

}