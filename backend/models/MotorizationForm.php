<?php
namespace backend\models;

use Yii;
use yii\base\Model;


/**
 * MotorizationForm form
 */
class MotorizationForm extends Model
{
    public $engine;
    public $cylinderCapacity;
    public $power;
    public $unit;
    public $unitList = ['CP' => 'CP', 'kW' => 'kW'];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['engine', 'cylinderCapacity', 'power'], 'required'],
            [['unit'], 'safe'],
            ['cylinderCapacity', 'isNumericCylinderCapacity'],
            ['power', 'isNumericPower']
        ];
    }

    public function isNumericCylinderCapacity(){
        if(!is_numeric($this->cylinderCapacity)){
            return $this->addError('cylinderCapacity', Yii::t('app', 'cylinderCapacityNumericError', [
                'cylinderCapacity' => $this->cylinderCapacity
            ]));
        }
    }

    public function isNumericPower(){
        if(!is_numeric($this->power)){
            return $this->addError('power', Yii::t('app', 'powerNumericError', [
                'power' => $this->power
            ]));
        }
    }

    public function attributeLabels(){
        return [
            'engine' => Yii::t('app', 'engineName'),
            'cylinderCapacity' => Yii::t('app', 'cylinderCapacity'),
            'power' => Yii::t('app', 'power'),
            'unit' => Yii::t('app', 'unit'),
        ];
    }

}