<?php

namespace backend\models\base;

use Yii;
use yii\base\Model;
use common\models\User;

class RegisterForm extends Model{

    public $name;
    public $password;
    public $passwordConfirm;

    public function rules(){
        return [
            [['name', 'password', 'passwordConfirm'], 'required'],
            ['passwordConfirm', 'passwordConfirmValidation'],
            ['password', 'string', 'min' => '8']
        ];
    }

    public function attributeLabels(){
        return [
            'name' => Yii::t('app', 'name'),
            'password' => Yii::t('app', 'password'),
            'passwordConfirm' => Yii::t('app', 'passwordConfirm')
        ];
    }

    public function passwordConfirmValidation(){
        if($this->password != $this->passwordConfirm){
            return $this->addError('passwordConfirm', Yii::t('app', 'passwordConfirmError'));
        }
    }

    public function create($user){
        $user->setPassword($this->password);
        $user->name = $this->name;
        $user->status = $user::STATUS_ACTIVE;
        $user->registerToken = null;
        if($user->save()){
            return true;
        }
    }
}

?>