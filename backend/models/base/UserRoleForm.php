<?php

namespace backend\models\base;

use Yii;
use yii\base\Model;
use common\models\User;
use backend\models\Role;
use backend\models\UserRole;

class UserRoleForm extends Model {

    public $id;
    public $email;
    public $originEmail;
    public $roles = [];

    public function rules() {
		return [
            [['email', 'roles'], 'required'],
            ['email', 'uniqueEmail'],
            ['roles', 'validRoles']
		];
    }
    
    public function uniqueEmail() {
        $user = User::find()->where(['email' => $this->email])->one();
		if ($user && $user->email != $this->originEmail) {
			return $this->addError('email', Yii::t('app', 'userUniqueEmailError', ['email' => $this->email]) . '!');
		}
    }

    public function validRoles() {
		foreach ($this->roles as $role) {
			$roleModel = Role::find()->where(['name' => $role])->one();
			if (!$roleModel) {
				return $this->addError('roles',
					Yii::t('app', 'invalidRoleError', ['role' => $role]) . '!');
			}
		}
	}

    public function attributeLabels() {
		return [
			'email' => Yii::t('app', 'E-mail'),
			'roles' => Yii::t('app', 'roles')
		];
    }
    
    public function createUserRoles($user = null) {
		if(!$user){
			$user = new User();
			// When create new user status set inative by default
			$user->status = $user::STATUS_INACTIVE;
		}
		$user->email = $this->email;
		if ($user->save(false)) {
			$this->id = $user->id;
			foreach ($this->roles as $role) {
				$userRolesModel = new UserRole();
				$role = Role::find()->where(['name' => $role])->one();
				if(!UserRole::find()->where(['userId' => $user->id, 'roleId' => $role->id])->one()){
					$userRolesModel->userId = $user->id;
					$userRolesModel->roleId = $role->id;
					$userRolesModel->save();
				}
			}
		}
		return true;
	}

	public function updateUserRoles($id) {
		$user = User::findOne($id);
		$this->id = $user->id;
		foreach($user->userRoles as $userRole){
			if(!in_array($userRole->role->name, $this->roles)){
				$userRole->delete();
			}
		}
		$this->createUserRoles($user);
		return true;
	}

	public function loadRolesByUserId($id) {
		$user = User::findOne($id);
		$this->email = $user->email;
		$this->id = $user->id;
		$this->originEmail = $user->email;
		foreach ($user->userRoles as $userRole) {
			$this->roles[] = $userRole->role->name;
		}
	}

}
