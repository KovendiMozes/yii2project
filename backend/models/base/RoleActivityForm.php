<?php

namespace backend\models\base;

use backend\models\Activity;
use Yii;
use yii\base\Model;
use backend\models\Role;
use backend\models\RoleActivity;

class RoleActivityForm extends Model {

	public $id;
	public $name;
	public $originName;
	public $activities = [];

	public function rules() {
		return [
			[['name', 'activities'], 'required'],
			['name', 'uniqueName'],
			['activities', 'validActivities']
		];
	}

	public function uniqueName() {
		$role = Role::find()->where(['name' => $this->name])->one();
		if ($role && $role->name != $this->originName) {
			return $this->addError('name', Yii::t('app', 'roleUniqueNameError', ['name' => $this->name]) . '!');
		}
	}

	public function validActivities() {
		foreach ($this->activities as $activity) {
			$activitiesModel = Activity::find()->where(['name' => $activity])->one();
			if (!$activitiesModel) {
				return $this->addError('activities',
					Yii::t('app', 'invalidActivityError', ['activity' => $activity]) . '!');
			}
		}
	}

	public function attributeLabels() {
		return [
			'name' => Yii::t('app', 'appellation'),
			'activities' => Yii::t('app', 'activities')
		];
	}

	public function createRoleActivities($role = null) {
		if(!$role){
			$role = new Role();
		}
		$role->name = $this->name;
		if ($role->save(false)) {
			$this->id = $role->id;
			foreach ($this->activities as $activity) {
				$roleActivitiesModel = new RoleActivity();
				$activity = Activity::find()->where(['name' => $activity])->one();
				if(!RoleActivity::find()->where(['roleId' => $role->id, 'activityId' => $activity->id])->one()){
					$roleActivitiesModel->roleId = $role->id;
					$roleActivitiesModel->activityId = $activity->id;
					$roleActivitiesModel->save();
				}
			}
		}
		return true;
	}

	public function updateRoleActivities($id) {
		$role = Role::findOne($id);
		$this->id = $role->id;
		foreach($role->roleActivities as $roleActivity){
			if(!in_array($roleActivity->activity->name, $this->activities)){
				$roleActivity->delete();
			}
		}
		$this->createRoleActivities($role);
		return true;
	}

	public function loadActivitiesByRoleId($id) {
		$role = Role::findOne($id);
		$this->name = $role->name;
		$this->id = $role->id;
		$this->originName = $role->name;
		foreach ($role->roleActivities as $roleActivity) {
			$this->activities[] = $roleActivity->activity->name;
		}
	}

}
