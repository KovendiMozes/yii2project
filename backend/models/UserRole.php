<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "userRoles".
 *
 * @property int $id
 * @property int $userId
 * @property int $roleId
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userRoles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'roleId'], 'required'],
            [['userId', 'roleId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'roleId' => 'Role ID',
        ];
    }

    public function getRole(){
        return $this->hasOne(Role::className(), ['id' => 'roleId']);
    }

    public function getRoleActivity(){
        return $this->hasMany(RoleActivity::className(), ['roleId' => 'roleId']);
    }
}
