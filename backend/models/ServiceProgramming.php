<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "serviceProgramming".
 *
 * @property int $id
 * @property int $workerId
 * @property int $workTypeId
 * @property string $overlapDays
 * @property string $start
 * @property string $end
 * @property string $createdAt
 * @property string $updatedAt
 */
class ServiceProgramming extends \yii\db\ActiveRecord
{
    public $timePickerStart;
    public $timePickerEnd;
    public $dateStart;
    public $dateEnd;
    public $dateNoOverlap;

    public $servicePeriodITPStart = 8;
    public $servicePeriodITPEnd = 16;

    public $servicePeriodSzervizStart = 12;
    public $servicePeriodSzervizEnd = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'serviceProgramming';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workerId', 'workTypeId', 'overlapDays', 'position', 'timePickerStart','timePickerEnd'], 'required'],
            [['position', 'overlapDays', 'start', 'end', 'createdAt', 'updatedAt', 'workerId', 'workTypeId'], 'safe'],
            [['timePickerStart', 'timePickerEnd', 'dateEnd', 'dateStart', 'dateNoOverlap'], 'dateValidation'],
        ];
    }

    private function dateReplace($date, $time = false){
        if(!$time){
            return str_replace(".","-",$date);
        }
        return str_replace(".","-",$date).' '.$time;
    }

    private function noOverlapDateValidation($today){
        $dateNoOverlapTime = strtotime($this->dateReplace($this->dateNoOverlap));
        if($dateNoOverlapTime < $today){
            $this->addError('dateNoOverlap', Yii::t('app','dateNoOverlapError', [
                'dateNoOverlap'=>$this->dateNoOverlap
            ]));
        }

        $start = strtotime($this->dateReplace($this->dateNoOverlap, $this->timePickerStart));
        $end = strtotime($this->dateReplace($this->dateNoOverlap, $this->timePickerEnd));

        if($start >= $end){
            $this->addError('timePickerEnd', Yii::t('app','timePickerEndError', [
                'timePickerEnd'=>$this->timePickerEnd
            ]));
        }

        if($this->position == "I.T.P"){
            $workStart = strtotime($this->dateReplace($this->dateNoOverlap, $this->servicePeriodITPStart.':00'));
            $workEnd = strtotime($this->dateReplace($this->dateNoOverlap, $this->servicePeriodITPEnd.':00'));
        }else{
            $workStart = strtotime($this->dateReplace($this->dateNoOverlap, $this->servicePeriodSzervizStart.':00'));
            $workEnd = strtotime($this->dateReplace($this->dateNoOverlap, $this->servicePeriodSzervizEnd.':00'));
        }

        if($start < $workStart || $start > $workEnd){
            $this->addError('timePickerStart', Yii::t('app','workStartError', [
                'timePickerStart'=>$this->timePickerStart
            ]));
        }
        if($end > $workEnd || $end < $workStart){
            $this->addError('timePickerEnd', Yii::t('app','workEndError', [
                'timePickerEnd'=>$this->timePickerEnd
            ]));
        }
    }

    private function overlapDateValidation($today){
        $dateStartTime = strtotime($this->dateReplace($this->dateStart));
        $dateEndTime = strtotime($this->dateReplace($this->dateEnd));

        if($dateEndTime < $today || $dateStartTime >= $dateEndTime){
            $this->addError('dateEnd', Yii::t('app','dateEndTimeError', [
                'dateEnd'=>$this->dateEnd
            ]));
        }

        if($dateStartTime < $today || $dateStartTime >= $dateEndTime){
            $this->addError('dateStart', Yii::t('app','dateStartError', [
                'dateStart'=>$this->dateStart
            ]));
        }

        $start = strtotime($this->dateReplace($this->dateStart, $this->timePickerStart));
        $end = strtotime($this->dateReplace($this->dateEnd, $this->timePickerEnd));

        if($start > $end){
            $this->addError('timePickerEnd', Yii::t('app','timePickerEndError', [
                'timePickerEnd'=>$this->timePickerEnd
            ]));
        }

        if($this->position == "I.T.P"){
            $workStartFirst = strtotime($this->dateReplace($this->dateStart, $this->servicePeriodITPStart.':00'));
            $workEndFirst = strtotime($this->dateReplace($this->dateStart, $this->servicePeriodITPEnd.':00'));
            $workStartSecond = strtotime($this->dateReplace($this->dateEnd, $this->servicePeriodITPStart.':00'));
            $workEndSecond = strtotime($this->dateReplace($this->dateEnd, $this->servicePeriodITPEnd.':00'));
        }else{
            $workStartFirst = strtotime($this->dateReplace($this->dateStart, $this->servicePeriodSzervizStart.':00'));
            $workEndFirst = strtotime($this->dateReplace($this->dateStart, $this->servicePeriodSzervizEnd.':00'));
            $workStartSecond = strtotime($this->dateReplace($this->dateEnd, $this->servicePeriodSzervizStart.':00'));
            $workEndSecond = strtotime($this->dateReplace($this->dateEnd, $this->servicePeriodSzervizEnd.':00'));
        }

        if($start < $workStartFirst || $start > $workEndFirst){
            $this->addError('timePickerStart', Yii::t('app','workStartError', [
                'timePickerStart'=>$this->timePickerStart
            ]));
        }
        if($end > $workEndSecond || $end < $workStartSecond){
            $this->addError('timePickerEnd', Yii::t('app','workEndError', [
                'timePickerEnd'=>$this->timePickerEnd
            ]));
        }
    }

    public function dateValidation(){

        $today = strtotime(date("Y-m-d", time()));

        if(!$this->overlapDays){
           $this->noOverlapDateValidation($today);
        }
        else{
            $this->overlapDateValidation($today);
        }
        return $this->getErrors();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'workerId' => Yii::t('app', 'worker'),
            'workTypeId' => Yii::t('app', 'workTypes'),
            'overlapDays' => Yii::t('app', 'overlapDays'),
            'start' => 'Start',
            'end' => 'End',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'position' => Yii::t('app', 'position'),
            'timePickerStart' => Yii::t('app', 'timePickerStart'),
            'timePickerEnd' => Yii::t('app', 'timePickerEnd'),
            'dateStart' => Yii::t('app', 'dateStart'),
            'dateEnd' => Yii::t('app', 'dateEnd'),
            'dateNoOverlap' => Yii::t('app', 'date'),
        ];
    }

    public function beforeSave($insert) {

        $this->workTypeId = WorkType::find()->where(['description' => $this->workTypeId])->one()->id;

        if($this->isNewRecord){
            $this->createdAt = date('Y-m-d H:i:s');
        }

        $this->updatedAt = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }

    public function getWorker(){
        return $this->hasOne(Worker::className(),['id' => 'workerId']);
    }

    public function getWorkerType(){
        return $this->hasOne(WorkType::className(),['id' => 'workTypeId']);
    }

    public static function setModelUpdate($model){
        $start = strtotime($model->start);
        $end = strtotime($model->end);

        $model->timePickerStart = date("h:i A", $start);
        $model->timePickerEnd = date("h:i A", $end);
        $model->dateStart = date("Y.m.d", $start);
        $model->dateEnd = date("Y.m.d", $end);
        $model->dateNoOverlap = date("Y.m.d", $start);

        return $model;
    }

    public static function createDate($date){
        $time = strtotime(str_replace(".","-",$date));
        $date = date("Y-m-d H:i:s", $time);

        return $date;
    }

    public static function setModel($form, $model){
        $model->position = $form["position"];
        $model->overlapDays = intval($form["overlapDays"]);

        if($model->overlapDays == 1){
            $model->end = $model::createDate($form["dateEnd"]." ".$form["timePickerEnd"]);
            $model->start = $model::createDate($form["dateStart"]." ".$form["timePickerStart"]);
        }else{
            $model->end = $model::createDate($form["dateNoOverlap"]." ".$form["timePickerEnd"]);
            $model->start = $model::createDate($form["dateNoOverlap"]." ".$form["timePickerStart"]);
        }

        return $model;
    }
}
