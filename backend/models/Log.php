<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property int $entityName
 * @property string $beforeChange
 * @property string $afterChange
 * @property int $userId
 * @property string $ip
 * @property string $createdAt
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entityName', 'action'], 'required'],
            [['userId'], 'integer'],
            [['beforeChange', 'afterChange'], 'string'],
            [['createdAt'], 'safe'],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entityName' => Yii::t('app', 'entityName'),
            'beforeChange' => Yii::t('app', 'beforeChange'),
            'afterChange' => Yii::t('app', 'afterChange'),
            'userId' => Yii::t('app', 'userName'),
            'ip' => 'Ip',
            'createdAt' => Yii::t('app', 'createdAt'),
            'action' => Yii::t('app', 'action'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->createdAt = date('Y-m-d H:i:s');
        $this->ip = self::getUserIp();
        $this->userId = Yii::$app->user->id;

        return parent::beforeSave($insert);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public static function getUserIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        }
        //whether ip is from proxy
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        //whether ip is from remote address
        else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }

    public static function twoArrayAreEquals($array1, $array2)
    {
        if (array_diff($array1, $array2) || array_diff($array2, $array1)) {
            return false;
        }
        return true;
    }

    /**
     * Create role log
     */
    public static function role($entityName, $newModel, $oldModel, $action)
    {
        $hasChange = false;
        $log = new self();
        $log->entityName = $entityName;
        $log->action = $action;
        // Set default empty string.
        $log->beforeChange = '';
        $log->afterChange = '';

        // Log for create
        if (!$oldModel) {
            $hasChange = true;
            foreach ($newModel as $field => $value) {
                if ($field == 'name') {
                    $log->afterChange .= Yii::t('app', $field) . ': ' . $value . ' | ';
                }
                if ($field == 'activities') {
                    $log->afterChange .= 'activities: ';
                    foreach ($value as $activity) {
                        $log->afterChange .= $activity . ', ';
                    }
                    $log->afterChange .= ' | ';
                }
            }
        } else {
            foreach ($oldModel as $field => $value) {
                if ($field == 'name' && $value != $newModel->$field) {
                    $log->beforeChange .= Yii::t('app', $field). ': ' . $value . ' | ';
                    $log->afterChange .= Yii::t('app', $field) . ': ' . $newModel->$field . ' | ';
                }
                if ($field == 'activities' && !self::twoArrayAreEquals($value, $newModel->$field)) {
                    $log->beforeChange .= 'activities: ';
                    $log->afterChange .= 'activities: ';
                    foreach ($value as $activity) {
                        $log->beforeChange .= $activity . ', ';
                    }
                    foreach ($newModel->$field as $activity) {
                        $log->afterChange .= $activity . ', ';
                    }
                    $log->beforeChange .= ' | ';
                    $log->afterChange .= ' | ';
                }
            }
            if ($log->afterChange) {
                $hasChange = true;
            }
        }

        if ($hasChange) {
            $log->beforeChange .= ' ID: <' . $newModel->id . '>';
            $log->afterChange .= ' ID: <' . $newModel->id . '>';
            $log->save();
        }
    }


    /**
     * Create car log
     */
    public static function car($entityName, $newModel, $oldModel, $action)
    {
        $hasChange = false;
        $log = new self();
        $log->entityName = $entityName;
        $log->action = $action;
        // Set default empty string.
        $log->beforeChange = '';
        $log->afterChange = '';

        // Log for create
        if (!$oldModel) {
            $hasChange = true;
            foreach ($newModel as $field => $value) {
                if($field == 'id'){
                    continue;
                }
                $log->afterChange .= Yii::t('app', $field) . ': ' . $value . ' | ';
            }
        } else {
            foreach ($oldModel as $field => $value) {
                if($field == 'id'){
                    continue;
                }
                if($value != $newModel[$field]){
                    $log->beforeChange .= Yii::t('app', $field) . ': ' . $value . ' | ';
                    $log->afterChange .= Yii::t('app', $field) . ': ' . $newModel[$field] . ' | ';
                }
            }
            if ($log->afterChange) {
                $hasChange = true;
            }
        }

        if ($hasChange) {
            if($oldModel){
                $log->beforeChange .= ' ID: <' . $newModel['id'] . '>';
            }else{
                $log->beforeChange .= ' ' . Yii::t('app', 'created');
            }
            $log->afterChange .= ' ID: <' . $newModel['id'] . '>';
            $log->save();
        }
    }
}
