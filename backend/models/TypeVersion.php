<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "typeVersions".
 *
 * @property int $id
 * @property string $name
 * @property int $brandTypeId
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class TypeVersion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'typeVersions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brandTypeId'], 'required'],
            [['userId'], 'integer'],
            [['createdAt', 'updatedAt', 'brandTypeId'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['name', 'uniqueName'],
        ];
    }


    public function uniqueName(){

        $model = TypeVersion::find()->where(['name'=>$this->name])->one();

        if($model){
            return $this->addError('name', Yii::t('app','typeVersionsUniqueNameError', [
                'name'=>$this->name
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'typeVersionName'),
            'brandTypeId' => Yii::t('app','brandType'),
            'userId' => Yii::t('app','userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->brandTypeId = BrandType::find()->where(['name' => $this->brandTypeId])->one()->id;
        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function getBrandType(){
        return $this->hasOne(BrandType::className(),['id' => 'brandTypeId']);
    }

    public static function getTypeVersionListByBrandTypeName($brandTypeId){
        $typeVersions = self::find()->select(['id' => 'id', 'name' => 'name'])->where(['brandTypeId' => $brandTypeId])->asArray()->all();
        return $typeVersions;
    }
}
