<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "OpenningHours".
 *
 * @property int $id
 * @property string $code
 * @property string $start
 * @property string $close
 * @property string $createdAt
 * @property string $updatedAt
 */
class OpenningHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'openningHours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'open' => 'Open',
            'close' => 'Close',
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

}
