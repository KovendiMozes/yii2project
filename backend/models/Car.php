<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int $brandId
 * @property int $brandTypeId
 * @property int $typeVersionId
 * @property int $carCategorieId
 * @property int $bodyworkId
 * @property int $carColorId
 * @property int $carEmissionId
 * @property int $carSeatNumberId
 * @property int $combustibleId
 * @property int $drivenTypeId
 * @property int $equipmentLevelId
 * @property int $gearboxId
 * @property int $statusId
 * @property int $userId
 * @property int $fuelSupplyId
 * @property string $pedigreeNumber
 * @property string $mileage
 * @property string $vintage
 * @property string $firstEntryDate
 * @property string $bodyNumber
 * @property string $licensePlate
 * @property string $motorization
 * @property string $createdAt
 * @property string $updatedAt
 */
class Car extends \yii\db\ActiveRecord
{

    public $brandName;
    public $carCategorieName;
    public $bodyworkName;
    public $carColorName;
    public $carEmissionName;
    public $carSeatNumber_;
    public $combustibleName;
    public $drivenTypeName;
    public $gearboxName;
    public $statusName;
    public $fuelSupplyName;

    /**
     * {@inheritdoc}
     */
    
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brandId', 'brandTypeId', 'typeVersionId', 'carCategorieId', 'bodyworkId', 'carColorId', 'carEmissionId', 'carSeatNumberId', 'combustibleId', 'drivenTypeId', 'equipmentLevelId', 'gearboxId', 'statusId', 'fuelSupplyId', 'pedigreeNumber', 'mileage', 'vintage', 'firstEntryDate', 'bodyNumber'], 'required'],
            [['createdAt', 'updatedAt', 'brandId', 'brandTypeId', 'typeVersionId', 'carCategorieId', 'bodyworkId', 'carColorId', 'carEmissionId', 'carSeatNumberId', 'combustibleId', 'drivenTypeId', 'equipmentLevelId', 'gearboxId', 'statusId', 'userId', 'fuelSupplyId', 'licensePlate', 'motorization'], 'safe'],
            [['mileage'], 'number', 'max' => 9999999, 'min' => 1],
            [['pedigreeNumber'], 'string', 'max' => 7, 'min' => 7],
            [['bodyNumber'], 'string', 'max' => 17 , 'min' => 17],
            [['vintage', 'firstEntryDate'], 'dateValidation'],
            [['brandName', 'carCategorieName', 'bodyworkName', 'carColorName', 'carEmissionName', 'carSeatNumber_', 'combustibleName', 'drivenTypeName', 'gearboxName', 'statusName', 'fuelSupplyName'], 'safe'],
        ];
    }

    private function ifErrorMessage($dateValidationValueTime, $start, $end, $messageName, $dateValidationValue){
        if($dateValidationValueTime < $start || $dateValidationValueTime > $end){
            $this->addError($messageName, Yii::t('app', $messageName.'Error', [
                $messageName => $dateValidationValue
            ]));
        }
    } 

    public function dateValidation(){
        $vintageTime = strtotime($this->vintage);
        $firstEntryDateTime = strtotime($this->firstEntryDate);
        $startTime = strtotime("1950-01-01");
        $endTime = strtotime(date('Y-m-d', time()));

        if($vintageTime > $firstEntryDateTime){
            $this->addError('firstEntryDate', Yii::t('app', 'dateError', [
                'firstEntryDate' => $this->firstEntryDate
            ]));
        }
    
        $this->ifErrorMessage($vintageTime, $startTime, $endTime, "vintage", $this->vintage);
        $this->ifErrorMessage($firstEntryDateTime, $startTime, $endTime, "firstEntryDate", $this->firstEntryDate);

        return $this->getErrors();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brandId' => Yii::t('app', 'brand'),
            'brandName' => Yii::t('app', 'brand'),
            'brandTypeId' => Yii::t('app', 'brandType'),
            'typeVersionId' => Yii::t('app', 'typeVersionName'),
            'carCategorieId' => Yii::t('app', 'carCategoryName'),
            'carCategorieName' => Yii::t('app', 'carCategoryName'),
            'bodyworkId' => Yii::t('app', 'bodyworkName'),
            'bodyworkName' => Yii::t('app', 'bodyworkName'),
            'carColorId' => Yii::t('app', 'carColorName'),
            'carColorName' => Yii::t('app', 'carColorName'),
            'carEmissionId' => Yii::t('app', 'carEmissionName'),
            'carEmissionName' => Yii::t('app', 'carEmissionName'),
            'carSeatNumberId' => Yii::t('app', 'carSeatNumber'),
            'carSeatNumber_' => Yii::t('app', 'carSeatNumber'),
            'combustibleId' => Yii::t('app', 'combustibleName'),
            'combustibleName' => Yii::t('app', 'combustibleName'),
            'drivenTypeId' => Yii::t('app', 'drivenTypeName'),
            'drivenTypeName' => Yii::t('app', 'drivenTypeName'),
            'equipmentLevelId' => Yii::t('app', 'equipmentLevelName'),
            'equipmentLevelName' => Yii::t('app', 'equipmentLevelName'),
            'gearboxId' => Yii::t('app', 'gearboxName'),
            'gearboxName' => Yii::t('app', 'gearboxName'),
            'statusId' => Yii::t('app', 'statusName'),
            'statusName' => Yii::t('app', 'statusName'),
            'userId' => Yii::t('app', 'userName'),
            'fuelSupplyId' => Yii::t('app', 'fuelSupplyName'),
            'fuelSupplyName' => Yii::t('app', 'fuelSupplyName'),
            'pedigreeNumber' => Yii::t('app', 'pedigreeNumber'),
            'mileage' => Yii::t('app', 'mileage'),
            'vintage' => Yii::t('app', 'vintage'),
            'firstEntryDate' => Yii::t('app', 'firstEntryDate'),
            'bodyNumber' => Yii::t('app', 'bodyNumber'),
            'licensePlate' => Yii::t('app', 'licensePlate'),
            'motorization' => Yii::t('app', 'motorization'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        if($this->isNewRecord){
            $this->createdAt = date('Y-m-d H:i:s');
            $this->brandId = Brand::find()->where(['name' => $this->brandId])->one()->id;
            $this->carCategorieId = CarCategory::find()->where(['name' => $this->carCategorieId])->one()->id;
            $this->bodyworkId = BodyWork::find()->where(['name' => $this->bodyworkId])->one()->id;
            $this->carColorId = CarColor::find()->where(['name' => $this->carColorId])->one()->id;
            $this->carEmissionId = CarEmission::find()->where(['name' => $this->carEmissionId])->one()->id;
            $this->carSeatNumberId = CarSeatNumber::find()->where(['number' => $this->carSeatNumberId])->one()->id; 
            $this->combustibleId = Combustible::find()->where(['name' => $this->combustibleId])->one()->id;
            $this->drivenTypeId = DrivenType::find()->where(['name' => $this->drivenTypeId])->one()->id;
            $this->gearboxId = Gearbox::find()->where(['name' => $this->gearboxId])->one()->id;
            $this->statusId = Status::find()->where(['name' => $this->statusId])->one()->id;
            $this->fuelSupplyId = FuelSupply::find()->where(['name' => $this->fuelSupplyId])->one()->id;
            $this->userId = Yii::$app->user->id;
        }else{
            // var_dump($this);exit;
            $this->brandId = Brand::find()->where(['name' => $this->brandName])->one()->id;
            $this->carCategorieId = CarCategory::find()->where(['name' => $this->carCategorieName])->one()->id;
            $this->bodyworkId = BodyWork::find()->where(['name' => $this->bodyworkName])->one()->id;
            $this->carColorId = CarColor::find()->where(['name' => $this->carColorName])->one()->id;
            $this->carEmissionId = CarEmission::find()->where(['name' => $this->carEmissionName])->one()->id;
            $this->carSeatNumberId = CarSeatNumber::find()->where(['number' => $this->carSeatNumber_])->one()->id; 
            $this->combustibleId = Combustible::find()->where(['name' => $this->combustibleName])->one()->id;
            $this->drivenTypeId = DrivenType::find()->where(['name' => $this->drivenTypeName])->one()->id;
            $this->gearboxId = Gearbox::find()->where(['name' => $this->gearboxName])->one()->id;
            $this->statusId = Status::find()->where(['name' => $this->statusName])->one()->id;
            $this->fuelSupplyId = FuelSupply::find()->where(['name' => $this->fuelSupplyName])->one()->id;
            $this->userId = Yii::$app->user->id;
        }

        $this->updatedAt = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }


    public function getUser(){
        return $this->hasOne(User::className(),['id' => 'userId']);
    }

    public function getBrand(){
        return $this->hasOne(Brand::className(),['id' => 'brandId']);
    }

    public function getBrandType(){
        return $this->hasOne(BrandType::className(),['id' => 'brandTypeId']);
    }

    public function getTypeVersion(){
        return $this->hasOne(TypeVersion::className(),['id' => 'typeVersionId']);
    }

    public function getEquipmentLevel(){
        return $this->hasOne(EquipmentLevel::className(),['id' => 'equipmentLevelId']);
    }

    public function getCarCategory(){
        return $this->hasOne(CarCategory::className(),['id' => 'carCategorieId']);
    }

    public function getBodyWork(){
        return $this->hasOne(BodyWork::className(),['id' => 'bodyworkId']);
    }

    public function getCarColor(){
        return $this->hasOne(CarColor::className(),['id' => 'carColorId']);
    }

    public function getCarEmission(){
        return $this->hasOne(CarEmission::className(),['id' => 'carEmissionId']);
    }

    public function getCarSeatNumber(){
        return $this->hasOne(CarSeatNumber::className(),['id' => 'carSeatNumberId']);
    }

    public function getCombustible(){
        return $this->hasOne(Combustible::className(),['id' => 'combustibleId']);
    }

    public function getDrivenType(){
        return $this->hasOne(DrivenType::className(),['id' => 'drivenTypeId']);
    }
        
    public function getGearbox(){
        return $this->hasOne(Gearbox::className(),['id' => 'gearboxId']);
    }
        
    public function getStatus(){
        return $this->hasOne(Status::className(),['id' => 'statusId']);
    }

    public function getFuelSupply(){
        return $this->hasOne(FuelSupply::className(),['id' => 'fuelSupplyId']);
    }

    public static function getTranslationKeysWithValues($model){
        return [
            'id' => $model->id,
            'brand' => $model->brand->name,
            'brandType' => $model->brandType->name,
            'typeVersionName' => $model->typeVersion->name,
            'equipmentLevelName' => $model->equipmentLevel->name,
            'carCategoryName' => $model->carCategory->name,
            'bodyworkName' => $model->bodyWork->name,
            'carColorName' => $model->carColor->name,
            'carEmissionName' => $model->carEmission->name,
            'carSeatNumber' => $model->carSeatNumber->number,
            'combustibleName' => $model->combustible->name,
            'drivenTypeName' => $model->drivenType->name,
            'gearboxName' => $model->gearbox->name,
            'statusName' => $model->status->name,
            'userName' => $model->user->name,
            'fuelSupplyName' => $model->fuelSupply->name,
            'pedigreeNumber' => $model->pedigreeNumber,
            'mileage' => $model->mileage,
            'vintage' => $model->vintage,
            'firstEntryDate' => $model->firstEntryDate,
            'bodyNumber' => $model->bodyNumber,
            'licensePlate' => $model->licensePlate,
            'motorization' => $model->motorization,
            // 'createdAt' => $model->createdAt,
            // 'updatedAt' => $model->updatedAt,
        ];
    }
}
