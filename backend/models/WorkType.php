<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "workTypes".
 *
 * @property int $id
 * @property string $description
 * @property int $duration
 * @property string $createdAt
 * @property string $updatedAt
 */
class WorkType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'workTypes';
    }

    public $durationList = [
        15 => '0:15', 30 => '0:30', 45 => '0:45', 60 => '1:00',
        75 => '1:15', 90 => '1:30', 105 => '1:45', 120 => '2:00',
        135 => '2:15', 150 => '2:30', 165 => '2:45', 180 => '3:00',
        195 => '3:15', 210 => '3:30', 225 => '3:45', 240 => '4:00',
        255 => '4:15', 270 => '4:30', 285 => '4:45', 300 => '5:00',
        315 => '5:15', 330 => '5:30', 345 => '5:45', 360 => '6:00',
        375 => '6:15', 390 => '6:30', 405 => '6:45', 420 => '7:00',
        435 => '7:15', 450 => '7:30', 465 => '7:45', 480 => '8:00'
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'duration'], 'required'],
            [['duration'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['description'], 'string', 'max' => 255],
            ['description', 'uniqueDescription']
        ];
    }

    public function uniqueDescription(){

        if($this->isNewRecord){
        
            $model = WorkType::find()->where(['description'=>$this->description])->one();

            if($model){
                return $this->addError('description', Yii::t('app','workTypeUniqueDescriptionError', [
                    'description'=>$this->description
                ]));
            }
        
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => Yii::t('app', 'description'),
            'duration' => Yii::t('app', 'duration'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        if($this->isNewRecord){
            $this->createdAt = date('Y-m-d H:i:s');
        }

        $this->updatedAt = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);

    }
}
