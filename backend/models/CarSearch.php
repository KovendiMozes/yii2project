<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Car;

/**
 * CarSearch represents the model behind the search form of `backend\models\Car`.
 */
class CarSearch extends Car
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['createdAt', 'updatedAt', 'brandId', 'brandTypeId', 'typeVersionId', 'carCategorieId', 'bodyworkId', 'carColorId', 'carEmissionId', 'carSeatNumberId', 'combustibleId', 'drivenTypeId', 'equipmentLevelId', 'gearboxId', 'statusId', 'userId', 'fuelSupplyId', 'mileage', 'pedigreeNumber', 'bodyNumber', 'vintage', 'firstEntryDate', 'licensePlate', 'motorization'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Car::find();
        $query->joinWith('user');
        $query->joinWith('brand');
        $query->joinWith('brandType');
        $query->joinWith('typeVersion');
        $query->joinWith('carCategory');
        $query->joinWith('equipmentLevel');
        $query->joinWith('bodyWork');
        $query->joinWith('carColor');
        $query->joinWith('carEmission');
        $query->joinWith('carSeatNumber');
        $query->joinWith('combustible');
        $query->joinWith('drivenType');
        $query->joinWith('gearbox');
        $query->joinWith('status');
        $query->joinWith('fuelSupply');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ]);

        $query->andFilterWhere([
            'like', 'user.name', $this->userId
        ]);

        $query->andFilterWhere([
            'like', 'brands.name', $this->brandId
        ]);

        $query->andFilterWhere([
            'like', 'brandTypes.name', $this->brandTypeId
        ]);

        $query->andFilterWhere([
            'like', 'typeVersions.name', $this->typeVersionId
        ]);

        $query->andFilterWhere([
            'like', 'equipmentLevels.name', $this->equipmentLevelId
        ]);

        $query->andFilterWhere([
            'like', 'carCategories.name', $this->carCategorieId
        ]);

        $query->andFilterWhere([
            'like', 'bodyworks.name', $this->bodyworkId
        ]);

        $query->andFilterWhere([
            'like', 'carColors.name', $this->carColorId
        ]);

        $query->andFilterWhere([
            'like', 'carEmissions.name', $this->carEmissionId
        ]);

        $query->andFilterWhere([
            'like', 'carSeatNumbers.number', $this->carSeatNumberId
        ]);

        $query->andFilterWhere([
            'like', 'combustibles.name', $this->combustibleId
        ]);

        $query->andFilterWhere([
            'like', 'drivenTypes.name', $this->drivenTypeId
        ]);

        $query->andFilterWhere([
            'like', 'gearboxes.name', $this->gearboxId
        ]);

        $query->andFilterWhere([
            'like', 'statuses.name', $this->statusId
        ]);

        $query->andFilterWhere([
            'like', 'fuelSupplies.name', $this->fuelSupplyId
        ]);

        $query->andFilterWhere([
            'like', 'cars.mileage', $this->mileage
        ]);

        $query->andFilterWhere([
            'like', 'cars.pedigreeNumber', $this->pedigreeNumber
        ]);

        $query->andFilterWhere([
            'like', 'cars.bodyNumber', $this->bodyNumber
        ]);

        $query->andFilterWhere([
            'like', 'cars.vintage', $this->vintage
        ]);

        $query->andFilterWhere([
            'like', 'cars.firstEntryDate', $this->firstEntryDate
        ]);

        $query->andFilterWhere([
            'like', 'cars.licensePlate', $this->licensePlate
        ]);

        $query->andFilterWhere([
            'like', 'cars.motorization', $this->motorization
        ]);

        return $dataProvider;
    }
}
