<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "workers".
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $position
 * @property string $telefonNumber
 * @property string $createdAt
 * @property string $updatedAt
 */
class Worker extends \yii\db\ActiveRecord
{

    public $fullName;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'workers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'position'], 'required'],
            [['createdAt', 'updatedAt', 'telefonNumber', 'fullName'], 'safe'],
            [['firstName', 'lastName', 'position', 'telefonNumber'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => Yii::t('app', 'firstName'),
            'lastName' => Yii::t('app', 'lastName'),
            'position' => Yii::t('app', 'position'),
            'telefonNumber' => Yii::t('app', 'telefonNumber'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
            'fullName' => Yii::t('app', 'fullName'),
        ];
    }

    public function beforeSave($insert) {
        
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public static function getWorkerListByPosition($position){
        $workers = self::find()->select(['id' => 'id','name' => 'CONCAT(firstName, " ",lastName)'])->where(['position' => $position])->asArray()->all();
        return $workers;
    }
}
