<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "carSeatNumbers".
 *
 * @property int $id
 * @property int $number
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class CarSeatNumber extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carSeatNumbers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['number', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'userId'], 'safe'],
            ['number', 'uniqueNumber']
        ];
    }

    public function uniqueNumber(){

        $model = CarSeatNumber::find()->where(['number'=>$this->number])->one();

        if($model){
            return $this->addError('number', Yii::t('app','carSeatNumbersUniqueNumberError', [
                'number'=>$this->number
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => Yii::t('app', 'carSeatNumber'),
            'userId' => Yii::t('app','userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
