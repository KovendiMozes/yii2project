<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property string $source
 * @property string $target
 * @property int $languageId
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class Translation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source', 'target', 'languageId'], 'required'],
            [['languageId', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'userId'], 'safe'],
            [['source', 'target'], 'string', 'max' => 255],
            [['source', 'languageId'], 'uniqueSource'],
            [['target', 'languageId'], 'uniqueTarget']
        ];
    }
    
    public function uniqueSource(){

        if($this->isNewRecord){

            $query = self::find()->where(['source'=>$this->source, 'languageId'=>$this->languageId])->one();

            if($query){
                return $this->addError('source', Yii::t('app','translationSourceUniqueCodeError', [
                    'source'=>$this->source
                ]));
            }

        }
    }

    public function uniqueTarget(){
        
        if(! $this->isNewRecord){

            $query = self::find()->where(['target'=>$this->target, 'languageId'=>$this->languageId])->one();

            if($query){
                return $this->addError('target', Yii::t('app','translationTargetUniqueCodeError', [
                    'target'=>$this->target
                ]));
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => Yii::t('app', 'translationSource'),
            'target' => Yii::t('app', 'translationTarget'),
            'languageId' => Yii::t('app', 'languageId'),
            'userId' => Yii::t('app','userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }


    public function beforeSave($insert) {

        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function getLanguage(){
        return $this->hasOne(Language::className(), ['id' => 'languageId']);
    }

}
