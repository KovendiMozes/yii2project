<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BrandType;

/**
 * BrandTypeSearch represents the model behind the search form of `backend\models\BrandType`.
 */
class BrandTypeSearch extends BrandType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'createdAt', 'updatedAt', 'brandId', 'userId'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrandType::find();
        $query->joinWith('user');
        $query->joinWith('brand');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ]);

        $query->andFilterWhere([
            'like', 'user.name', $this->userId
        ]);

        $query->andFilterWhere([
            'like', 'brands.name', $this->brandId
        ]);

        $query->andFilterWhere([
            'like', 'brandTypes.name', $this->name
        ]);

        return $dataProvider;
    }
}
