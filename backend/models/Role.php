<?php

namespace backend\models;

use common\models\User;
use backend\models\Log;
use Yii;

/**
 * This is the model class for table "roles".
 *
 * @property int $id
 * @property int $name
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class Role extends \yii\db\ActiveRecord{

    public $activityList;
    /**
     * {@inheritdoc}
     */
    public static function tableName(){
        return 'roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(){
        return [
            [['name'], 'required'],
            [['createdAt', 'updatedAt', 'userId', 'activityList'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'appellation'),
            'userId' => Yii::t('app', 'userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
            'activityList' => Yii::t('app', 'activityList')
        ];
    }

    public function beforeSave($insert) {

        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public function getRoleActivities(){
        return $this->hasMany(RoleActivity::className(), ['roleId' => 'id']);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function all(){
        return self::find()->orderBy(['name' => SORT_ASC])->all();
    }
}
