<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "engines".
 *
 * @property int $id
 * @property string $name
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class Engine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'engines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['userId'], 'integer'],
            [['createdAt', 'updatedAt', 'userId'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['name', 'uniqueName']
        ];
    }

    public function uniqueName(){

        $model = self::find()->where(['name'=>$this->name])->one();

        if($model){
            return $this->addError('name', Yii::t('app','engineUniqueNameError', [
                'name'=>$this->name
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'engineName'),
            'userId' => Yii::t('app','userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->userId = Yii::$app->user->id;
        if($this->isNewRecord){
            $this->createdAt = date('Y-m-d H:i:s');
        }

        $this->updatedAt = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);

    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
