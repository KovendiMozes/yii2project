<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $code
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['userId'], 'integer'],
            [['createdAt', 'updatedAt', 'userId'], 'safe'],
            [['code'], 'string', 'max' => 255],
            ['code', 'uniqueCode']
        ];
    }

    public function uniqueCode(){

        $model = Language::find()->where(['code'=>$this->code])->one();

        if($model){
            return $this->addError('code', Yii::t('app','uniqueCodeError', [
                'code'=>$this->code
            ]));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => Yii::t('app', 'code'),
            'userId' => Yii::t('app','userName'),
            'createdAt' => Yii::t('app', 'createdAt'),
            'updatedAt' => Yii::t('app', 'updatedAt'),
        ];
    }

    public function beforeSave($insert) {

        $this->userId = Yii::$app->user->id;
        $this->createdAt = date('Y-m-d H:i:s');
        $this->updatedAt = $this->createdAt;

        return parent::beforeSave($insert);

    }

    public  function getUser(){
        return $this->hasOne(User::className(),['id' => 'userId']);
    }
}
