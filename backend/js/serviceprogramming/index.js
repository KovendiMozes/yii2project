class ServiceProgramming {
	constructor() {
		this.dataId;

		var that = this;

		$(document).on('click', 'div[data-id]', function () {
			that.dataId = $(this).attr('data-id');
			$('#myModal').modal('show');
			$.ajax({
				url: '/backend/serviceprogramming/update?id=' + that.dataId,
				type: 'GET',
				async: true,
				success: function (data) {
					$('#myModal .modal-body').html(data);
				},
			});
		});

		$('#button').on('click', function () {
			$('#myModal').modal('show');
			$.ajax({
				url: '/backend/serviceprogramming/create',
				type: 'GET',
				success: function (data) {
					$('#myModal .modal-body').html(data);
				},
			});
		});

		$(document).ready(function () {
			that.programming();
		});

		$('#serviceprogramming-search').on('click' ,function () {
			that.programming();
		});
	}

	getDataId() {
		return this.dataId;
	}

	programming() {
		var date = $('#indexDate').val();

		$.ajax({
			type: 'POST',
			url: '/backend/serviceprogramming/programming',
			data: { date: date },
			success: function (data) {
				$('#programming').html(data);
			},
		});
	}
}

var serviceProgramming = new ServiceProgramming();
