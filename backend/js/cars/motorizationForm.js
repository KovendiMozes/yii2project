class MototrizationForm{

    constructor(){

        var that = this;

        $(document).on('beforeSubmit', '#engine-form', function(){
            var form = $(this);
            if(form.find('.has-error').length) {
                return false;
            }else{
                var motorization = that.inputsPutTogether();
                $("#car-motorization").val(motorization);
                $('#car-modal').modal('hide');
            }

            return false;
        });

        $(document).on('focusout', '#motorizationform-cylindercapacity', function(){
            var cylinderCapacity = parseInt($('#motorizationform-cylindercapacity').val());
            
            that.rounding(cylinderCapacity);
        });
        
    }

    inputsPutTogether(){
        var engine = $('#motorizationform-engine').val();
        var cylinderCapacity = $('#motorizationform-cylindercapacity').val();
        var power = $('#motorizationform-power').val();
        var unit = $('#motorizationform-unit').val();

        var motorization = cylinderCapacity.concat(" ", engine, " ", power, " ", unit);;

        return motorization;
    }

    rounding(cylinderCapacity){
        var divide = cylinderCapacity % 100;
        var result = 0;
        if(divide >= 50){
            result = ((cylinderCapacity-divide+100)/1000);
            this.lengthString(result);
        }else{
            result = (cylinderCapacity-divide)/1000;
            this.lengthString(result);
        }
    }

    lengthString(value){
        var string = value.toString();
        if(string.length == 1){
            $('#motorizationform-cylindercapacity').val(string.concat('.0'));
        }else{
            $('#motorizationform-cylindercapacity').val(string);
        }
    }

}

var mototrizationForm = new MototrizationForm();