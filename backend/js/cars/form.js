class Cars{

    constructor(){

        var isNewRecord = $("#cars-form").attr("isnewrecord");
        var that = this;
        var licensePlate = '';
        var motorization = '';
        
        $('#car-licenseplate').on('click', function () {
            $('#car-modal').modal('show');
            if(isNewRecord != "true"){
                licensePlate = $('#car-licenseplate').val();
            }
            $.ajax({
				url: '/backend/cars/licenseplateform',
                type: 'POST',
                async: true,
                data: {licenseplate: licensePlate},
				success: function (data) {
                    $('#car-modal .modal-body').html(data);
                    setTimeout(function() {
                        $("#licenseplateform-county").focus();
                    }, 1000)
				},
            });
            
        });

        $('#car-motorization').on('click', function () {
            $('#car-modal').modal('show');
            if(isNewRecord != "true"){
                motorization = $('#car-motorization').val();
            }
            $.ajax({
				url: '/backend/cars/motorizationform',
                type: 'POST',
                async: true,
                data: {motorization: motorization},
				success: function (data) {
                    $('#car-modal .modal-body').html(data);
				},
            });
            
        });

    }

}

var cars = new Cars();