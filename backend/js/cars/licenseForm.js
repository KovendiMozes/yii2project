class LicenseForm{

    constructor(){

        var that = this;

        $(document).on('beforeSubmit', '#license-plate-form', function(){
            var form = $(this);
            if(form.find('.has-error').length) {
                return false;
            }else{
                var licensePlate = that.inputsPutTogether();
                $("#car-licenseplate").val(licensePlate);
                $('#car-modal').modal('hide');
            }

            return false;
        })
    }

    inputsPutTogether(){
        var county = $("#licenseplateform-county").val();
        county = county.toUpperCase();
        var twoDigitNumber = $("#licenseplateform-twodigitnumber").val();
        var threeLetters = $("#licenseplateform-threeletters").val();
        threeLetters = threeLetters.toUpperCase();

        var licensePlate = county.concat(" ", twoDigitNumber, " ", threeLetters);

        return licensePlate;
    }

}

var licenseForm = new LicenseForm();