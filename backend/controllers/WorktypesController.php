<?php

namespace backend\controllers;

use Yii;
use common\components\BaseController;
use backend\models\WorkType;
use backend\models\WorkTypeSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * WorktypesController implements the CRUD actions for WorkType model.
 */
class WorktypesController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function(){
                    throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage')); 
                },
                'rules' => [
                    [
                        'actions' => ['create', 'index', 'update', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                            if(Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)){
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['gettimes', 'searchdescription', 'searchduration'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorkType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WorkType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WorkType();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WorkType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGettimes(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['results' => ['start' => '', 'end' => '', 'status' => 0, 'dateEnd' => '', 'dateStart' => ""]];

        $workTypePost = Yii::$app->request->post();

        $workTypeDescription = $workTypePost['workTypeDescription'];
        $servicePeriodStart = intval($workTypePost['servicePeriodStart']);
        $servicePeriodEnd = intval($workTypePost['servicePeriodEnd']);
        $overlapDays = $workTypePost['overlapDays'];

        $dateNoOverlap = strtotime(str_replace(".","-",$workTypePost['dateNoOverlap']));
        $dateStart = strtotime(str_replace(".","-",$workTypePost['dateStart']));
        $dateEnd = strtotime(str_replace(".","-",$workTypePost['dateEnd']));
        $timePickerStart = $workTypePost['timePickerStart'];

        $workTypeDuration = WorkType::find()->select(['duration' => 'duration'])->where(['description' => $workTypeDescription])->asArray()->all();
        $workTypeDurationInteger = intval($workTypeDuration[0]['duration']);

        if($overlapDays == "true"){
            $nowTime = strtotime((date("Y-m-d", $dateStart))." ".$timePickerStart);
            $workDate = date("Y-m-d", $dateStart);
        }
        else{
            $nowTime = strtotime((date("Y-m-d", $dateNoOverlap))." ".$timePickerStart);
            $workDate = date("Y-m-d", $dateNoOverlap);
        }

        $workTime = $nowTime + $workTypeDurationInteger * 60;
        $dateTime = $dateStart;
        $endWorkTime = strtotime($workDate." ".$servicePeriodEnd.":00");

        if($workTime <= $endWorkTime){
            $startTime = date("h:i A", $nowTime);
            $endTime = date("h:i A", $workTime);
        }
        else{
            $date = date("Y-m-d", strtotime("+1 day", $dateTime));
            if(intval(date("H", $nowTime)) == $servicePeriodEnd){
                $startTime = date("h:i A", strtotime($date." ".$servicePeriodStart.":00"));
                $response['results']['dateStart'] = str_replace("-", ".", date("Y-m-d", (strtotime($date." ".$servicePeriodStart.":00"))));
            }else{
                $startTime = date("h:i A", $nowTime);
            }

            $overlapTime = $workTypeDurationInteger - (($endWorkTime - $nowTime)/60);

            while($overlapTime > ((strtotime($date." ".$servicePeriodEnd.":00") - strtotime($date." ".$servicePeriodStart.":00"))/60)){
                $overlapTime = $overlapTime - (((strtotime($date." ".$servicePeriodEnd.":00")) - strtotime($date." ".$servicePeriodStart.":00"))/60);
                $dateTime = strtotime($date);
                $date = date("Y-m-d", strtotime("+1 day", $dateTime));
            }

            $endTime = date("h:i A", (strtotime($date." ".$servicePeriodStart.":00") + $overlapTime * 60));
            $response['results']['status'] = 1;
            $response['results']['dateEnd'] = str_replace("-", ".", date("Y-m-d", (strtotime($date." ".$servicePeriodStart.":00"))));
        }

        $response['results']['start'] = $startTime;
        $response['results']['end'] = $endTime;

        return $response;
    }

    /**
     * Finds the WorkType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearchdescription($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = WorkType::find()
				->select('description as id, description as text')
				->where(['like', 'description', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }
    
    public function actionSearchduration($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = WorkType::find()
				->select('duration as id, duration as text')
				->where(['like', 'duration', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
	}
}
