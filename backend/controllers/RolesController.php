<?php

namespace backend\controllers;

use backend\models\Activity;
use backend\models\base\RoleActivityForm;
use backend\models\Log;
use backend\models\Role;
use backend\models\RoleSearch;
use common\components\BaseController;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * RolesController implements the CRUD actions for Roles model.
 */
class RolesController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function () {
                    throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage'));
                },
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)) {
                                return true;
                            }
                            return false;
                        },
                    ],
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Roles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Roles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoleActivityForm();
        $allActivities = Activity::all();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->createRoleActivities()) {
            Log::role('roles', $model, null, 'create');
            Yii::$app->session->setFlash('success', Yii::t('app', 'saved') . '!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'allActivities' => $allActivities,
        ]);
    }

    /**
     * Updates an existing Roles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = new RoleActivityForm();
        $model->loadActivitiesByRoleId($id);
        $oldModel = new RoleActivityForm();
        $oldModel->loadActivitiesByRoleId($id);
        $allActivities = Activity::all();
        var_dump($allActivities);exit;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->updateRoleActivities($id)) {
            Log::role('roles', $model, $oldModel, 'update');
            Yii::$app->session->setFlash('success', Yii::t('app', 'saved') . '!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'allActivities' => $allActivities,
        ]);
    }

    /**
     * Deletes an existing Roles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'deleted') . '!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Roles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Role the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearch($value = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($value)) {
            $model = Role::find()
                ->select('name as id, name as text')
                ->where(['like', 'name', $value])
                ->limit(10)->asArray()->all();

            $response['results'] = $model;
        }
        return $response;
    }
}
