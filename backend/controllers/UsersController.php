<?php

namespace backend\controllers;

use common\components\BaseController;
use common\models\User;
use common\models\UserSearch;
use backend\models\base\UserRoleForm;
use backend\models\Role;
use backend\models\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;
use backend\models\base\RegisterForm;
use yii\web\ForbiddenHttpException;

/**
 * RolesController implements the CRUD actions for Roles model.
 */
class UsersController extends BaseController
{
	/**
	 * {@inheritdoc}
	 */

	public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function(){
                    throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage')); 
                },
				'rules' => [
					[
						'actions' => ['create', 'view', 'index', 'update', 'delete'],
						'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                            if(Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)){
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['search', 'searchbyemail'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['?']
                    ]
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
	}
	
	public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRoleForm();
        $allRoles = Role::all();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->createUserRoles()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'allRoles' => $allRoles
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = new UserRoleForm();
		$model->loadRolesByUserId($id);
		$allRoles = Role::all();

		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if ($model->load(Yii::$app->request->post()) && $model->updateUserRoles($id)) {
			Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
			return $this->redirect(['view', 'id' => $model->id]);
		}

        return $this->render('update', [
			'model' => $model,
			'allRoles' => $allRoles
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

	public function actionSearch($value = null){
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = User::find()
				->select('name as id, name as text')
				->where(['like', 'name', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }
    
    public function actionSearchbyemail($value = null){
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = User::find()
				->select('email as id, email as text')
				->where(['like', 'email', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }
    
    public function actionRegister(){
        if(!isset($_GET['token']) || !$_GET['token']){
            return $this->redirect(['/']);
        }
        $user = User::find()->where(['registerToken' => $_GET['token']])->one();
        
        if(!$user){
            return $this->redirect(['/']);
        }

        $registerForm = new RegisterForm();

        if(Yii::$app->request->isAjax && $registerForm->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($registerForm);
        }

        if($registerForm->load(Yii::$app->request->post()) && $registerForm->create($user)){
            Yii::$app->session->setFlash('success', Yii::t('app', 'registeredSuccessfullyMessage').'!');
            return $this->redirect(['site/login']);
        }

        return $this->render('register', [
            'user' => $user,
            'registerForm' => $registerForm
        ]);
    }
}
