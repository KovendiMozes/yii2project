<?php

namespace backend\controllers;

use Yii;
use common\components\BaseController;
use backend\models\Log;
use backend\models\Car;
use backend\models\CarSearch;
use backend\models\BrandType;
use backend\models\TypeVersion;
use backend\models\EquipmentLevel;
use backend\models\LicensePlateForm;
use backend\models\MotorizationForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * CarsController implements the CRUD actions for Car model.
 */
class CarsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function(){
                    throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage')); 
                },
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                            if(Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)){
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['brandtypelist', 'typeversionlist', 'equipmentlevellist', 'searchmileage', 'searchpedigreenumber', 'searchbodynumber', 'licenseplateform', 'motorizationform'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Car();

        $model->vintage = date("Y-m-d", time());
        $model->firstEntryDate = date("Y-m-d", time());

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $newModel = Car::getTranslationKeysWithValues($model);
            Log::car('cars', $newModel, null, 'create');
			Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldModel = Car::getTranslationKeysWithValues($model);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $newModel = Car::getTranslationKeysWithValues($model);
            Log::car('cars', $newModel, $oldModel, 'update');
			Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Car model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Car::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function relationData($relation, $out){
        if(isset($_GET['isNewRecord']) && $_GET['isNewRecord'] == 0){
            $model = $this->findModel($_GET['carId']);
            return ['output'=>$out, 'selected'=>$model->$relation->id];
        }
        return ['output'=>$out, 'selected'=>$out[0]];
    }


    public function actionBrandtypelist() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $brandName = $parents[0];
                $out = BrandType::getBrandTypeListByBrandName($brandName);
                // var_dump($out);exit;
                return $this->relationData('brandType', $out);
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionTypeversionlist() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents']) && !empty($_POST['depdrop_parents'][0])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $brandTypeId = $parents[0];
                $out = TypeVersion::getTypeVersionListByBrandTypeName($brandTypeId);

                return $this->relationData('typeVersion', $out);
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionEquipmentlevellist() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $brandName = $parents[0];
                $out = EquipmentLevel::getEquipmentLevelListByBrandName($brandName); 

                return $this->relationData('equipmentLevel', $out);
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionSearchmileage($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = Car::find()
				->select('mileage as id, mileage as text')
				->where(['like', 'mileage', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }
    
    public function actionSearchpedigreenumber($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = Car::find()
				->select('pedigreeNumber as id, pedigreeNumber as text')
				->where(['like', 'pedigreeNumber', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }
    
    public function actionSearchbodynumber($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = Car::find()
				->select('bodyNumber as id, bodyNumber as text')
				->where(['like', 'bodyNumber', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
    }

    public function actionLicenseplateform(){
        $model = new LicensePlateForm();

        if(isset($_POST['licenseplate']) && !empty($_POST['licenseplate'])){
            $explodeLicensePlate = explode(' ',$_POST['licenseplate']);
            $model->county = $explodeLicensePlate[0];
            $model->twoDigitNumber = $explodeLicensePlate[1];
            $model->threeLetters = $explodeLicensePlate[2];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        return $this->renderAjax('licensePlateForm', [
            'model' => $model,
        ]);
    }

    public function actionMotorizationform(){
        $model = new MotorizationForm();

        if(isset($_POST['motorization']) && !empty($_POST['motorization'])){
            $explodeMotorization = explode(' ',$_POST['motorization']);
            $model->engine = $explodeMotorization[1];
            $model->cylinderCapacity = $explodeMotorization[0];
            $model->power = $explodeMotorization[2];
            $model->unit = $explodeMotorization[3];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        return $this->renderAjax('motorizationForm', [
            'model' => $model,
        ]);
    }
    
}
