<?php

namespace backend\controllers;

use common\components\BaseController;
use backend\models\Brand;
use common\models\User;
use Yii;
use backend\models\BrandType;
use backend\models\BrandTypeSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * BrandtypeController implements the CRUD actions for BrandType model.
 */
class BrandtypeController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function(){
                    throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage')); 
                },
				'rules' => [
					[
						'actions' => ['create', 'view', 'index', 'update', 'delete'],
						'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action){
                            if(Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)){
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['search'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BrandType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $users = User::find()->asArray()->all();

        $brands = Brand::find()->asArray()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
            'brands' => $brands
        ]);
    }

    /**
     * Displays a single BrandType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BrandType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BrandType();

        $brands = Brand::find()->asArray()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'brands' => $brands
        ]);
    }

    /**
     * Updates an existing BrandType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $brands = Brand::find()->asArray()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'saved').'!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'brands' => $brands
        ]);
    }

    /**
     * Deletes an existing BrandType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'deleted').'!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the BrandType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BrandType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BrandType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearch($value = null) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$response = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($value)) {
			$model = BrandType::find()
				->select('name as id, name as text')
				->where(['like', 'name', $value])
				->limit(10)->asArray()->all();

			$response['results'] = $model;
		}
		return $response;
	}
}
