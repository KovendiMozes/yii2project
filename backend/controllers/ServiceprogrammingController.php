<?php

namespace backend\controllers;

use Yii;
use common\components\BaseController;
use backend\models\ServiceProgramming;
use backend\models\Worker;
use backend\models\_Constants;
use backend\models\ServiceProgrammingSearch;
use backend\models\OpenningHours;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ServiceprogrammingController implements the CRUD actions for ServiceProgramming model.
 */
class ServiceprogrammingController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            // 'access' => [
            //     'class' => AccessControl::className(),
            //     'denyCallback' => function(){
            //         throw new ForbiddenHttpException(Yii::t('app', 'forbiddenMessage')); 
            //     },
            //     'rules' => [
            //         [
            //             'actions' => ['create', 'index', 'update'],
            //             'allow' => true,
            //             'roles' => ['@'],
            //             'matchCallback' => function($rule, $action){
            //                 if(Yii::$app->protect->isEnabled($action->controller->module->requestedRoute)){
            //                     return true;
            //                 }
            //                 return false;
            //             }
            //         ],
            //         [
            //             'actions' => ['programming', 'workerlist', 'timevalidation', 'updaterecord', 'createrecord'],
            //             'allow' => true,
            //             'roles' => ['@']
            //         ]
            //     ],
            // ],
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => (YII_ENV_PROD) ? [''] : ['http://localhost:3000'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'POST', 'PUT'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['X-Wsse', 'Content-Type'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServiceProgramming models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceProgrammingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ServiceProgramming model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServiceProgramming();
        $model->dateStart = date('Y.m.d');
        $model->dateEnd = date('Y.m.d', strtotime('+1 day', time()));
        $model->dateNoOverlap = date('Y.m.d');

		Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			return ActiveForm::validate($model);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionCreaterecord(){

        $model = new ServiceProgramming();

        if ($model->load(Yii::$app->request->post())) {
            $serialize = Yii::$app->request->post();
            $form = $serialize["ServiceProgramming"];

            $model = $model::setModel($form, $model);

            if($model->save()){
                return true;
            }
        }
        return false;
    }

    /**
     * Updates an existing ServiceProgramming model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model = $model::setModelUpdate($model);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdaterecord($id){
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $serialize = Yii::$app->request->post();
            $form = $serialize["ServiceProgramming"];

            $model = $model::setModel($form, $model);

            if($model->save()){
                return true;
            }
        }
        return false;
    }

    public function actionModals()
    {
        
        return $this->renderAjax('modals');
    }

    /**
     * Finds the ServiceProgramming model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServiceProgramming the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceProgramming::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function relationData($relation, $out){
        if(isset($_GET['isNewRecord']) && $_GET['isNewRecord'] == 0){
            $model = $this->findModel($_GET['workerId']);
            return ['output'=>$out, 'selected'=>$model->$relation->id];
        }
        return ['output'=>$out, 'selected'=>''];
    }


    public function actionWorkerlist() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $position = $parents[0];
                $out = Worker::getWorkerListByPosition($position); 
                
                return $this->relationData('worker', $out);
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionTimevalidation(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['timePickerStart' => '', 'timePickerEnd' => '', 'status' => ['start' => false, 'end' => false]];

        $data = Yii::$app->request->post();
        
        $timePickerStart = $data["timePickerStart"];
        $timePickerEnd = $data["timePickerEnd"];
        $servicePeriodStart = $data["servicePeriodStart"];
        $servicePeriodEnd = $data["servicePeriodEnd"];

        $round_numerator = 60 * 15;
        $nowTime = ( round ( time() / $round_numerator ) * $round_numerator ) + (60 * 60);

        $date = date("Y-m-d", $nowTime);

        $workStartTime = strtotime($date." ".$servicePeriodStart.":00");
        $workEndTime = strtotime($date." ".$servicePeriodEnd.":00");

        $pickerStartTime = strtotime($date." ".$timePickerStart);
        $pickerEndTime = strtotime($date." ".$timePickerEnd);

        if($workStartTime > $pickerStartTime){
            $response['timePickerStart'] = date("h:i A", $workStartTime);
            $response['status']['start'] = true;
        }else if($workEndTime < $pickerStartTime){
            $response['timePickerStart'] = date("h:i A", $workEndTime);
            $response['status']['start'] = true;
        }
        if($workEndTime < $pickerEndTime){
            $response['timePickerEnd'] = date("h:i A", $workEndTime);
            $response['status']['end'] = true;
        }else if($workStartTime > $pickerEndTime){
            $response['timePickerEnd'] = date("h:i A", $workStartTime);
            $response['status']['end'] = true;
        }

        return $response;
    }

    private function hours(){
        $model = new ServiceProgramming;

        $start = $model->servicePeriodITPStart < $model->servicePeriodSzervizStart ? $model->servicePeriodITPStart : $model->servicePeriodSzervizStart;
        $end = $model->servicePeriodITPEnd > $model->servicePeriodSzervizEnd ? $model->servicePeriodITPEnd : $model->servicePeriodSzervizEnd;

        $startTime = strtotime(date("Y-m-d", time()).' '.$start.':00');
        $endTime = strtotime(date("Y-m-d", time()).' '.$end.':00');

        $hours = [];

        while($startTime <= $endTime){
            $hours += [date("H:i", $startTime) => []];
            $startTime += _Constants::QUARTER;
        }

        return $hours;
    }

    private function data($work){
        $worker = Worker::find()->select(['name' => 'CONCAT(firstName, " ",lastName)'])->where(['id' => $work->workerId])->asArray()->all();

        $data = [
            'workerName' => $worker[0]['name'],
        ];

        return $data;
    }

    private function workStartOrEnd($hours, $startOrEnd){
        foreach($hours as $key => $value){
            if($startOrEnd == 'start'){
                $result = $key;
                break;
            }
            $result = $key;
        }
        return $result;
    }

    private function split($start, $end, $hours, $overlapDays, $position, $dataDate){
        $endWork = $this->workStartOrEnd($hours, 'end');
        $dataDate = str_replace(".","-",$dataDate);

        $model = new ServiceProgramming;
        $servicePeriodStart = $position == 'I.T.P' ? $model->servicePeriodITPStart : $model->servicePeriodSzervizStart;
        $servicePeriodEnd = $position == 'I.T.P' ? $model->servicePeriodITPEnd : $model->servicePeriodSzervizEnd;

        if($overlapDays == 1){
            if(strtotime(date("Y-m-d", strtotime($end))) != strtotime($dataDate) && strtotime(date("Y-m-d", strtotime($start))) != strtotime($dataDate)){
                $endTime = strtotime(date("Y-m-d", strtotime($dataDate)).' '.$servicePeriodEnd.':00');
                $startTime = strtotime(date("Y-m-d", strtotime($dataDate)).' '.$servicePeriodStart.':00');
            }
            else{
                if(strtotime(date("Y-m-d", strtotime($start))) == strtotime($dataDate)){
                    $startTime = strtotime($start);
                    $endTime = strtotime(date("Y-m-d", strtotime($dataDate)).' '.$servicePeriodEnd.':00');
                }else{
                    $startTime = strtotime(date("Y-m-d", strtotime($dataDate)).' '.$servicePeriodStart.':00');
                    $endTime = strtotime($end);
                }
            }
        }
        else{
            $endTime = strtotime($end);
            $startTime = strtotime($start);
        }

        $hoursWork = [];
        $i = 0;

        while($startTime < $endTime){
            $hoursWork[$i] = date("H:i", $startTime);
            if($hoursWork[$i] == $endWork){
                break;
            }
            $startTime += _Constants::QUARTER;
            $i++;
        }
        
        return $hoursWork;
    }

    private function loadTimes($timeline, $hours, $work){
        $result = false;
        foreach($timeline['hours'] as $key => $value){
            foreach($hours as $value2){
                if($key == $value2){
                    $result = true;
                    break;
                }else{
                    $result = false;
                }
            }
            if($result == true){
                $timeline['hours'][$key][] = $work->id;
            }else{
                $timeline['hours'][$key][] = 0;
            }
        }

        return $timeline;
    }

    public function actionProgramming(){
        $data = Yii::$app->request->post();
        $dateTime = strtotime(str_replace(".","-",$data['date']))-(60*60);

        $model = ServiceProgramming::find()->where("UNIX_TIMESTAMP(DATE_FORMAT(START, '%Y-%m-%d')) <= ".$dateTime." AND UNIX_TIMESTAMP(DATE_FORMAT(END, '%Y-%m-%d')) >= ".$dateTime)->all();

        $hours = $this->hours();

        $timeline = [];

        foreach($model as $work){
            if(isset($timeline[$work->workerId])){
                $splitWorkHours = $this->split($work->start, $work->end, $hours, $work->overlapDays, $work->position, $data['date']);
                $timeline[$work->workerId] = $this->loadTimes($timeline[$work->workerId], $splitWorkHours, $work);
            }else{
                $timeline[$work->workerId] = [
                    'worker' => $this->data($work),
                    'hours' => $hours
                ];
                $splitWorkHours = $this->split($work->start, $work->end, $hours, $work->overlapDays, $work->position, $data['date']);
                $timeline[$work->workerId] = $this->loadTimes($timeline[$work->workerId], $splitWorkHours, $work);
            }
        }

        return $this->renderAjax('programming', [
            'timeline' => $timeline,
            'hours' => $hours,
        ]);
    }

// react
    public function actionDatabase(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $date = $_GET['date'];

        $dateTime = strtotime($date);

        $model = ServiceProgramming::find()->where("UNIX_TIMESTAMP(DATE_FORMAT(START, '%Y-%m-%d')) <= ".$dateTime." AND UNIX_TIMESTAMP(DATE_FORMAT(END, '%Y-%m-%d')) >= ".$dateTime)->all();

        return $model;
    }

    public function actionGetallopeninghours(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = OpenningHours::find()->all();

        return $model;
    }
 
    public function actionGetopeninghourbyposition(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $position = $_GET['position'];

        $model = OpenningHours::find()->where(['like', 'code', $position])->all();

        return $model;
    }

    public function actionGetworkerbyid(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $workerId = $_GET['id'];

        $model = Worker::find()->where(['id' => $workerId])->asArray()->all();

        return $model;
    }

}
