<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarEmission */

$this->title = Yii::t('app', 'createCarEmission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'carEmissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-emission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
