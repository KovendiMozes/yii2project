<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CarSeatNumber */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'carSeatNumbers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="car-seat-number-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number',
            [
                'attribute' => 'userId',
                'value' => function($model){
                    return $model->user->name;
                }
            ],
            'createdAt',
            'updatedAt',
        ],
    ]) ?>

</div>
