<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarSeatNumber */

$this->title = Yii::t('app', 'update'). ': ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'carSeatNumbers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'update');
?>
<div class="car-seat-number-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
