<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FuelSupply */

$this->title = Yii::t('app', 'fuelSupplyCreate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'fuelSupplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fuel-supply-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
