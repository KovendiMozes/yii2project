<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FuelSupply */

$this->title = Yii::t('app', 'update'). ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'fuelSupplies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'update');
?>
<div class="fuel-supply-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
