<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Engine */

$this->title = Yii::t('app', 'engineCreate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'engines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="engine-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
