<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\EngineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="engine-search search-box">

    <div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
]); ?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'name',
    'url' => Url::to(['engines/search']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'userId',
    'url' => Url::to(['users/search']),
])?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <?=Html::submitButton(Yii::t('app', 'search'), ['class' => 'btn btn-primary'])?>
                <a href="<?=Url::to(['index'])?>" class="btn btn-default btn-outline-secondary"><?=Yii::t('app', 'reset')?></a>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
