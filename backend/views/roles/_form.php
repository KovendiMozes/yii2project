<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roles-form">

    <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
]);?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Megnevezés', 'autocomplete' => 'off'])?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=\common\widgets\MultipleSelect2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'activities',
    'data' => $allActivities,
]);?>
        </div>
    </div>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
