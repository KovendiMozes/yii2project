<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="roles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'deleteConfirmationMessage'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'activityList',
                'value' => function($model) {
                    $activities = '';
                    foreach($model->roleActivities as $roleActivity) {
                        $activities .= $roleActivity->activity->name.'; ';
                    }
                    return $activities;
                }
            ],
            [
                'attribute' => 'userId',
                'value' => function($model) {
                    return $model->user->name;
                }
            ],
            'createdAt',
            'updatedAt',
        ],
    ]) ?>

</div>
