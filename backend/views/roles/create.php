<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */

$this->title = Yii::t('app', 'createRole');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'allActivities' => $allActivities
    ]) ?>

</div>
