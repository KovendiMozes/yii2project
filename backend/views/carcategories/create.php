<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarCategory */

$this->title = Yii::t('app', 'createCarCategory');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'carCategories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
