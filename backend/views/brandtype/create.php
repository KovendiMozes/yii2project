<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BrandType */

$this->title = Yii::t('app', 'createBrandType');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'brandTypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'brands' => $brands
    ]) ?>

</div>
