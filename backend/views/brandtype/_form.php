<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BrandType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-type-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'id' => 'brand-type-form',
    ]); ?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?php $model->isNewRecord ?  : $model->brandId = $model->brand->name; ?>
                <?= \common\widgets\Select2::widget([
                    'form' => $form,
                    'model' => $model,
                    'attribute' => $model->isNewRecord ? 'brandId' : 'brandId',
                    'url' => Url::to(['brands/search'])
                ]); ?>
            </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('name'), 'autocomplete' => 'off']) ?>
            </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
