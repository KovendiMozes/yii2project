<?php 
    $columns = Yii::$app->menu->list();
?>
<div class="row menu">
    <?php foreach($columns as $categories){ ?>
    <div class="col-lg-4">
        <?php foreach($categories as $label => $category){ ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="icon-container"><i class="<?= $category['icon'] ?>"></i></div> 
                <?= Yii::t('app', $label) ?></div>
            <div class="panel-body">
                <ul>
                    <?php foreach($category['items'] as $item){ ?>
                        <li>
                            <a href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</div>
