<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarColor */

$this->title = Yii::t('app', 'createCarColor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'carColors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
