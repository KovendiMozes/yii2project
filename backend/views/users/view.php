<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserRole */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-role-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'deleteConfirmationMessage'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => $model->getAttributeLabel('email'),
                'value' => $model->email
            ],
            [
                'label' => Yii::t('app', 'roles'),
                'value' => function($model) {
                    $roles = '';
                    foreach ($model->userRoles as $userrole) {
                        $roles .= $userrole->role->name.'; ';
                    }
                    return $roles;
                }
            ],
            [
                'label' => $model->getAttributeLabel('status'),
                'value' => function($model) {
                    switch ($model->status) {
                        case 9:
                            return Yii::t('app', 'inactiveUserStatus');
                        break;
                        case 10:
                            return Yii::t('app', 'activeUserStatus');
                        break;
                        case 0:
                            return Yii::t('app', 'deletedUserStatus');
                        break;
                    }
                    return;
                }
            ],
            'createdAt',
            'updatedAt'
        ],
    ]) ?>

</div>
