<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'userRegister');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true
    ]); ?>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
        <p class="alert alert-info"><?= Yii::t('app', 'userRegisterInformation').'.'; ?></p>
        <div class="form-group">
            <label for="" class="control-label"><?= $registerForm->getAttributeLabel('email') ?></label>
            <div><?= $user->email ?></div>
        </div>
        <?= $form->field($registerForm, 'name')->textInput(['autocomplete' => 'off', 'placeholder' => $registerForm->getAttributeLabel('name')]); ?>
        <?= $form->field($registerForm, 'password')->passwordInput(['autocomplete' => 'off', 'placeholder' => $registerForm->getAttributeLabel('password')]); ?>
        <?= $form->field($registerForm, 'passwordConfirm')->passwordInput(['autocomplete' => 'off', 'placeholder' => $registerForm->getAttributeLabel('passwordConfirm')]); ?>

        <div class="form-group">
            <button class="btn btn-primary"><?= Yii::t('app', 'register'); ?></button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

