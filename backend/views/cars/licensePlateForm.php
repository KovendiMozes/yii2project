<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="license-plate-form">

    <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>true,
            'id' => 'license-plate-form',
    ]); ?>

    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'county')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('county'), 'autocomplete' => 'off', 'style' => "text-transform:uppercase"]) ?>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'twoDigitNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('twoDigitNumber'), 'autocomplete' => 'off', 'style' => "text-transform:uppercase"]) ?>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'threeLetters')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('threeLetters'), 'autocomplete' => 'off', 'style' => "text-transform:uppercase"]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'insert'), [ 'id' => 'license-plate-form-insert', 'class' => 'btn btn-success']) ?>
    </div>

    <?php $this->registerJsFile('backend/js/cars/licenseForm.js',  ['depends' => [yii\web\JqueryAsset::className()]]) ?>

    <?php ActiveForm::end(); ?>

</div>