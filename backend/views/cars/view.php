<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Car */

$this->title = $model->brand->name." ".$model->brandType->name." - ".$model->typeVersion->name." ".$model->equipmentLevel->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="car-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'brandId',
                'value' => function($model){
                    return $model->brand->name;
                }
            ],
            [
                'attribute' => 'brandTypeId',
                'value' => function($model){
                    return $model->brandType->name;
                }
            ],
            [
                'attribute' => 'typeVersionId',
                'value' => function($model){
                    return $model->typeVersion->name;
                }
            ],
            [
                'attribute' => 'equipmentLevelId',
                'value' => function($model){
                    return $model->equipmentLevel->name;
                }
            ],
            [
                'attribute' => 'carCategorieId',
                'value' => function($model){
                    return $model->carCategory->name;
                }
            ],
            [
                'attribute' => 'combustibleId',
                'value' => function($model){
                    return $model->combustible->name;
                }
            ],
            [
                'attribute' => 'carColorId',
                'value' => function($model){
                    return $model->carColor->name;
                }
            ],
            [
                'attribute' => 'bodyworkId',
                'value' => function($model){
                    return $model->bodyWork->name;
                }
            ],
            [
                'attribute' => 'carEmissionId',
                'value' => function($model){
                    return $model->carEmission->name;
                }
            ],
            [
                'attribute' => 'drivenTypeId',
                'value' => function($model){
                    return $model->drivenType->name;
                }
            ],
            [
                'attribute' => 'carSeatNumberId',
                'value' => function($model){
                    return $model->carSeatNumber->number;
                }
            ],
            [
                'attribute' => 'gearboxId',
                'value' => function($model){
                    return $model->gearbox->name;
                }
            ],
            [
                'attribute' => 'statusId',
                'value' => function($model){
                    return $model->status->name;
                }
            ],
            [
                'attribute' => 'fuelSupplyId',
                'value' => function($model){
                    return $model->fuelSupply->name;
                }
            ],
            'mileage',
            'licensePlate',
            'motorization',
            [
                'attribute' => 'userId',
                'value' => function($model){
                    return $model->user->name;
                }
            ],
            'createdAt',
            'updatedAt',

        ],
    ]) ?>

</div>
