<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\DatePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'id' => 'cars-form',
    'options' => ['isnewrecord' => $model->isNewRecord ? 'true' : 'false'],
    ]); ?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->brandName = $model->brand->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'brandId' : 'brandName',
                'url' => Url::to(['brands/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'brandTypeId',
                'url' => Url::to(['cars/brandtypelist', 'isNewRecord' => $model->isNewRecord, 'carId' => $model->id]),
                'depend' => $model->isNewRecord ? ['car-brandid'] : ['car-brandname'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'typeVersionId',
                'url' => Url::to(['cars/typeversionlist', 'isNewRecord' => $model->isNewRecord, 'carId' => $model->id]),
                'depend' => ['car-brandtypeid'],
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'equipmentLevelId',
                'url' => Url::to(['cars/equipmentlevellist', 'isNewRecord' => $model->isNewRecord, 'carId' => $model->id]),
                'depend' => $model->isNewRecord ? ['car-brandid'] : ['car-brandname'],
            ]); ?>
        </div>
    </div>

    <hr>
  
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->carCategorieName = $model->carCategory->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'carCategorieId' : 'carCategorieName',
                'url' => Url::to(['carcategories/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->bodyworkName = $model->bodyWork->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'bodyworkId' : 'bodyworkName',
                'url' => Url::to(['bodyworks/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->carColorName = $model->carColor->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'carColorId' : 'carColorName',
                'url' => Url::to(['carcolors/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->carSeatNumber_ = $model->carSeatNumber->number; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'carSeatNumberId' : 'carSeatNumber_',
                'url' => Url::to(['carseatnumbers/search']),
            ]); ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->carEmissionName = $model->carEmission->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'carEmissionId' : 'carEmissionName',
                'url' => Url::to(['caremissions/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->statusName = $model->status->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'statusId' : 'statusName',
                'url' => Url::to(['statuses/search']),
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->combustibleName = $model->combustible->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'combustibleId' : 'combustibleName',
                'url' => Url::to(['combustibles/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->drivenTypeName = $model->drivenType->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'drivenTypeId' : 'drivenTypeName',
                'url' => Url::to(['driventypes/search']),
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->gearboxName = $model->gearbox->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'gearboxId' : 'gearboxName',
                'url' => Url::to(['gearboxes/search']),
            ]); ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php $model->isNewRecord ?  : $model->fuelSupplyName = $model->fuelSupply->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => $model->isNewRecord ? 'fuelSupplyId' : 'fuelSupplyName',
                'url' => Url::to(['fuelsupplies/search']),
            ]); ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'mileage')->textInput(['type' => 'number', 'placeholder' => $model->getAttributeLabel('mileage'), 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'pedigreeNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pedigreeNumber'), 'autocomplete' => 'off']) ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'bodyNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pedigreeNumber'), 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'vintage')->widget(DatePicker::classname(), ['pluginOptions' => ['format' => 'yyyy-mm-dd']])?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'firstEntryDate')->widget(DatePicker::classname(), ['pluginOptions' => ['format' => 'yyyy-mm-dd']])?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'licensePlate')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('XX NN YYY'), 'autocomplete' => 'off']) ?>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'motorization')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('motorization'), 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $this->registerJsFile('backend/js/cars/form.js',  ['depends' => [yii\web\JqueryAsset::className()]]) ?>

</div>


<?php 

echo $this->render('carModal');

?>