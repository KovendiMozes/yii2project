<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="engine-form">

    <?php $form = ActiveForm::begin([
            'enableAjaxValidation'=>true,
            'id' => 'engine-form',
    ]); ?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'engine',
                'url' => Url::to(['engines/search']),
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?= $form->field($model, 'cylinderCapacity')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('CylinderCapacity'), 'autocomplete' => 'off']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?= $form->field($model, 'power')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('power'), 'autocomplete' => 'off']) ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
            <?= $form->field($model, 'unit')->dropDownList($model->unitList) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'insert'), [ 'id' => 'license-plate-form-insert', 'class' => 'btn btn-success']) ?>
    </div>

    <?php $this->registerJsFile('backend/js/cars/motorizationForm.js',  ['depends' => [yii\web\JqueryAsset::className()]]) ?>

    <?php ActiveForm::end(); ?>

</div>