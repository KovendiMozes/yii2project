<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\CarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-search search-box">

    <div class="row">
        <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]);?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'brandId',
    'url' => Url::to(['brands/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'brandTypeId',
    'url' => Url::to(['brandtype/search']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'typeVersionId',
    'url' => Url::to(['typeversions/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'equipmentLevelId',
    'url' => Url::to(['equipmentlevels/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'carCategorieId',
    'url' => Url::to(['carcategories/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'bodyworkId',
    'url' => Url::to(['bodyworks/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'carColorId',
    'url' => Url::to(['carcolors/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'carEmissionId',
    'url' => Url::to(['caremissions/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'carSeatNumberId',
    'url' => Url::to(['carseatnumbers/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'combustibleId',
    'url' => Url::to(['combustibles/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'drivenTypeId',
    'url' => Url::to(['driventypes/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'gearboxId',
    'url' => Url::to(['gearboxes/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'statusId',
    'url' => Url::to(['statuses/search']),
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'userId',
    'url' => Url::to(['users/search']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'fuelSupplyId',
    'url' => Url::to(['fuelsupplies/search']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'mileage',
    'url' => Url::to(['cars/searchmileage']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'pedigreeNumber',
    'url' => Url::to(['cars/searchpedigreenumber']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'bodyNumber',
    'url' => Url::to(['cars/searchbodynumber']),
])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <?=$form->field($model, 'vintage')->widget(DatePicker::classname(), ['pluginOptions' => ['format' => 'yyyy-mm-dd']])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <?=$form->field($model, 'firstEntryDate')->widget(DatePicker::classname(), ['pluginOptions' => ['format' => 'yyyy-mm-dd']])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <?= $form->field($model, 'licensePlate')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'search') . '...', 'autocomplete' => 'off']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <?= $form->field($model, 'motorization')->textInput(['maxlength' => true, 'placeholder' => Yii::t('app', 'search') . '...', 'autocomplete' => 'off']) ?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <?=Html::submitButton(Yii::t('app', 'search'), ['class' => 'btn btn-primary'])?>
                <a href="<?=Url::to(['index'])?>" class="btn btn-default btn-outline-secondary"><?=Yii::t('app', 'reset')?></a>
            </div>
        </div>

        <?php ActiveForm::end();?>
    </div>

</div>
