<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'cars');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'createCar'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'brandId',
                'content' => function($model){
                    return $model->brand->name;
                }
            ],
            [
                'attribute' => 'brandTypeId',
                'content' => function($model){
                    return $model->brandType->name;
                }
            ],
            [
                'attribute' => 'typeVersionId',
                'content' => function($model){
                    return $model->typeVersion->name;
                }
            ],
            [
                'attribute' => 'equipmentLevelId',
                'content' => function($model){
                    return $model->equipmentLevel->name;
                }
            ],
            [
                'attribute' => 'carCategorieId',
                'content' => function($model){
                    return $model->carCategory->name;
                }
            ],
            // [
            //     'attribute' => 'combustibleId',
            //     'content' => function($model){
            //         return $model->combustible->name;
            //     }
            // ],
            // [
            //     'attribute' => 'carColorId',
            //     'content' => function($model){
            //         return $model->carColor->name;
            //     }
            // ],
            // [
            //     'attribute' => 'bodyworkId',
            //     'content' => function($model){
            //         return $model->bodyWork->name;
            //     }
            // ],
            // [
            //     'attribute' => 'carEmissionId',
            //     'content' => function($model){
            //         return $model->carEmission->name;
            //     }
            // ],
            // [
            //     'attribute' => 'drivenTypeId',
            //     'content' => function($model){
            //         return $model->drivenType->name;
            //     }
            // ],
            // [
            //     'attribute' => 'carSeatNumberId',
            //     'content' => function($model){
            //         return $model->carSeatNumber->number;
            //     }
            // ],
            // [
            //     'attribute' => 'gearboxId',
            //     'content' => function($model){
            //         return $model->gearbox->name;
            //     }
            // ],
            // [
            //     'attribute' => 'statusId',
            //     'content' => function($model){
            //         return $model->status->name;
            //     }
            // ],
            [
                'attribute' => 'userId',
                'content' => function($model){
                    return $model->user->name;
                }
            ],
            'createdAt',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
        ],
    ]); ?>


</div>
