<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ServiceProgramming */

$this->title = Yii::t('app', 'createServiceProgramming');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'serviceProgramming'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-programming-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
