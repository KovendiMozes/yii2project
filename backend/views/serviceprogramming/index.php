<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ServiceProgrammingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'serviceProgramming');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-programming-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id="root">
    
    </div>

</div>

<?php 

echo $this->render('modal');

?>
