<?php

use kartik\time\TimePicker;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ServiceProgramming */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-programming-form">

    <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'id' => 'service-programming-form',
    'options' => ['isnewrecord' => $model->isNewRecord ? 'true' : 'false'],
]);?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'position')->dropDownList(['I.T.P' => 'I.T.P', 'szerviz' => 'Szervíz'], ['prompt' => $model->getAttributeLabel('position')])?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'workerId',
    'url' => Url::to(['serviceprogramming/workerlist', 'isNewRecord' => $model->isNewRecord, 'workerId' => $model->id]),
    'depend' => ['serviceprogramming-position'],
])?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'workTypeId',
    'value' => !$model->isNewRecord ? $model->workerType->description : '',
    'url' => Url::to(['worktypes/searchdescription']),
])?>
        </div>
    </div>

    <div class="row" >
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'overlapDays')->checkBox()?>
        </div>
    </div>

    <div id = "dateTime">
        <div class = "row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <?=$form->field($model, 'dateNoOverlap')->widget(DatePicker::classname())?>
            </div>
        </div>

        <div class = "row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <?=$form->field($model, 'dateStart')->widget(DatePicker::classname())?>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <?=$form->field($model, 'dateEnd')->widget(DatePicker::classname())?>
            </div>
        </div>

        <div class = "row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <?=$form->field($model, 'timePickerStart')->widget(TimePicker::classname())?>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <?=$form->field($model, 'timePickerEnd')->widget(TimePicker::classname())?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?=Html::submitButton('Save', ['class' => 'btn btn-success', 'id' => 'submit'])?>
    </div>

    <?php ActiveForm::end();?>

</div>

<?php

$form = <<< JS

    var isNewRecord = $("#service-programming-form").attr("isnewrecord");

    var positionStatus, checkboxStatus;

    var dateTimeShow = function() {
        if (positionStatus && checkboxStatus) {
            $("#dateTime").show();
            $("#dateTime").children().eq(1).show();
            $("#dateTime").children().eq(0).hide();
        } else if(!checkboxStatus && positionStatus){
            $("#dateTime").show();
            $("#dateTime").children().eq(0).show();
            $("#dateTime").children().eq(1).hide();
        }else if(!positionStatus && checkboxStatus || !positionStatus){
            $('#dateTime').hide();
        }
    }

    if(isNewRecord == "true"){
        $('#dateTime').hide()
    }
    if(isNewRecord == "false"){
        positionStatus = true
        if($('#serviceprogramming-overlapdays').is(':checked')) {
            checkboxStatus = true;
            dateTimeShow();
        }else{
            checkboxStatus = false;
            dateTimeShow();
        }
    }

    $(document).ready(
        $('#service-programming-form').on('beforeSubmit', function(event, jqXHR, settings) {
            var form = $(this);
            var url;
            if(isNewRecord == 'false'){
                var id = serviceProgramming.getDataId();
                url = '/backend/serviceprogramming/updaterecord?id='+id;
            }
            else{
                url ='/backend/serviceprogramming/createrecord';
            }
            if(form.find('.has-error').length) {
                return false;
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: form.serialize(),
                success: function(data) {
                    $('#myModal').modal('hide');
                    serviceProgramming.programming();
                }
            });

            return true;
        }),
    );

    $('#serviceprogramming-position').change(function() {
        positionStatus = false;
        if ($(this).val()){
            positionStatus = true;
        }
        dateTimeShow();
    });

    $('#serviceprogramming-overlapdays').on('click', function() {
        checkboxStatus = false;
        if($(this).is(':checked')) {
            checkboxStatus = true;
            alert("Átfedés");
        }
        dateTimeShow();
    });

    $("#serviceprogramming-worktypeid, #serviceprogramming-datestart, #serviceprogramming-datenooverlap, #serviceprogramming-timepickerstart").change(function(){
        if($("#serviceprogramming-position").val() == "I.T.P"){
            var servicePeriodStart = "$model->servicePeriodITPStart";
            var servicePeriodEnd = "$model->servicePeriodITPEnd";
        }else{
            var servicePeriodStart = "$model->servicePeriodSzervizStart";
            var servicePeriodEnd = "$model->servicePeriodSzervizEnd";
        }

        var dateEnd = $('#serviceprogramming-dateend').val();
        var dateStart = $('#serviceprogramming-datestart').val();
        var dateNoOverlap = $('#serviceprogramming-datenooverlap').val();

        overlapDays = false;
        if($("#serviceprogramming-overlapdays").is(':checked')) {
            overlapDays = true;
        }

        var workTypeDescription = $("#serviceprogramming-worktypeid").val();
        var timePickerStart = $("#serviceprogramming-timepickerstart").val();

        var data = {workTypeDescription: workTypeDescription, servicePeriodStart: servicePeriodStart, servicePeriodEnd: servicePeriodEnd, dateNoOverlap: dateNoOverlap, overlapDays: overlapDays, dateEnd: dateEnd, dateStart: dateStart, timePickerStart: timePickerStart};

        $.ajax({
            type: "POST",
            url: "/backend/worktypes/gettimes",
            async: false,
            data: data,
            success: function (data){
                if(data.results.status != 0){
                    if(data.results.dateStart != ""){
                        $("#serviceprogramming-datestart").val(data.results.dateStart);
                    }
                    $("#serviceprogramming-dateend").val(data.results.dateEnd);
                    $("#serviceprogramming-overlapdays").prop( "checked", true );
                    checkboxStatus = true;
                    dateTimeShow();
                }else{
                    checkboxStatus = false;
                    $("#serviceprogramming-overlapdays").prop( "checked", false );
                    dateTimeShow();
                }
                $("#serviceprogramming-timepickerstart").val(data.results.start);
                $("#serviceprogramming-timepickerend").val(data.results.end);
            }
        });
    });
JS;

$this->registerJs($form);

?>