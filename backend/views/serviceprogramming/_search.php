<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\ServiceProgrammingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-programming-search search-box">
    <div class="row">

        <div id = "serviceprogramming-index-search" class="col-lg-2 col-md-4 col-sm-4 col-xs-8" style = "margin-bottom: 10px">
            <p>
                <b><?= Yii::t('app', 'date') ?></b>
            </p>
            <?php echo DatePicker::widget([
                'id'=>'indexDate',
                'name' => 'indexDate',
                'value' => date("Y.m.d", time()),
                'pluginOptions' => [
                    'format' => 'yyyy.mm.dd',
                    'todayHighlight' => true,
                    'todayBtn' => true, 
                ]
            ]);?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'search'), ['class' => 'btn btn-primary', 'id' => 'serviceprogramming-search']) ?>
            </div>
        </div>

    </div>
</div>
