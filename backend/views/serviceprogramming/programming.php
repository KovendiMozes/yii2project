<div class = "timeline">
    <div class="time-row">
        <?php foreach ($hours as $time => $empty) {?>
            <div class="quarter">
                <div class="time"><?=$time?></div>
                <div class="vertical-line"></div>
            </div>
        <?php }?>
    </div>
    <?php foreach ($timeline as $personKey => $person) {?>
        <div class="time-row">
            <div class="worker">
                <?php foreach ($person['worker'] as $worker => $workerInfo) {?>
                        <div class="worker-info"> <?= $workerInfo ?> </div>
                <?php }?>
            </div>
            <?php foreach ($person['hours'] as $time => $works) {?>
                <div class="quarter">
                    <div class="vertical-line"></div>
                    <?php foreach ($works as $workKey => $workTime) {?>
                        <div <?=$workTime != 0 ? 'data-id="' . $workTime . '"' : ''?> class="work-box <?= $workTime != 0 ? 'data' : ''?> <?=$workTime == 0 ? 'inactive' : 'active'?>"></div>
                    <?php }?>
                </div>
            <?php }?>
        </div>
    <?php }?>
</div>
