<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($model->isNewRecord){ ?>

        <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('description'), 'autocomplete' => 'off']) ?>

    <?php }else{ ?>

        <div class="form-group">
            <label class="control-label" for="description"><?= Yii::t('app', 'description') ?></label>
            <div><b><?= Html::encode($model->description) ?></b></div>
        </div>

    <?php } ?>

    <?= $form->field($model, 'duration')->dropDownList($model->durationList, ['prompt' => $model->getAttributeLabel('duration')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
