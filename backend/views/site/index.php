<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div id="site-index" class="body-content">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <?= $this->render('//layouts/menu/menu'); ?>
            </div>
        </div>
    </div>
</div>
