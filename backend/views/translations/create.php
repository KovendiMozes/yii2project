<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Translation */

$this->title = Yii::t('app', 'createTranslation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'languages' => $languages,
    ]) ?>

</div>
