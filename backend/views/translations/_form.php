<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Translation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translation-form">

        <?php $form = ActiveForm::begin(); ?>
        
        <?php if($model->isNewRecord){ ?>

            <?= $form->field($model, 'source')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('source'), 'autocomplete' => 'off']) ?>

            <?= $form->field($model, 'languageId')->dropDownList(\yii\helpers\ArrayHelper::map($languages,'id','code'), ['prompt' => $model->getAttributeLabel('languageId')]) ?>

        <?php }else{ ?>

            <div class="form-group">
                <label class="control-label" for="translation-source"><?= Yii::t('app', 'translationSource') ?></label>
                <div><b><?= Html::encode($model->source) ?></b></div>
            </div>

            <div class="form-group">
                <label class="control-label" for="translation-languageId"><?= Yii::t('app', 'languageId') ?></label>
                <div><b><?= Html::encode($model->language->code) ?></b></div>
            </div>

        <?php } ?>

        <?= $form->field($model, 'target')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('target'), 'autocomplete' => 'off']) ?>

        <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

</div>
