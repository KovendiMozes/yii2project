<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Translation */

$this->title = Yii::t('app', 'update'). ': ' . $model->source;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->source, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'update');
?>
<div class="translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
