<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Combustible */

$this->title = Yii::t('app', 'createCombustible');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'combustibles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="combustible-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
