<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TypeVersion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-version-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'id' => 'type-version-form',
    ]); ?>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?php $model->isNewRecord ?  : $model->brandTypeId = $model->brandType->name; ?>
            <?= \common\widgets\Select2::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'brandTypeId',
                'url' => Url::to(['brandtype/search'])
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('name'), 'autocomplete' => 'off']) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
