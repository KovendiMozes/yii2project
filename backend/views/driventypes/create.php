<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DrivenType */

$this->title = Yii::t('app', 'createDrivenType');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'drivenTypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driven-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
