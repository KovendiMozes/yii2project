<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Worker */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstName')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('firstName'), 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'lastName')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('lastName'), 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'position')->dropDownList(['I.T.P' => 'I.T.P', 'szerviz' => 'Szervíz'], ['prompt' => $model->getAttributeLabel('position')]) ?>

    <?= $form->field($model, 'telefonNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('telefonNumber'), 'autocomplete' => 'off']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
