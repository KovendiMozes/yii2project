<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Worker */

$this->title = Yii::t('app', 'update'). ': '. $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'update');
?>
<div class="worker-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
