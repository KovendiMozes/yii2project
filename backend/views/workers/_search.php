<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\WorkerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-search search-box">

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?= $form->field($model, 'fullName')->textInput(['placeholder' => $model->getAttributeLabel('fullName'), 'autocomplete' => 'off'])?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?= $form->field($model, 'position')->dropDownList(['I.T.P' => 'I.T.P', 'szerviz' => 'Szervíz'], ['prompt' => $model->getAttributeLabel('position')])?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'search'), ['class' => 'btn btn-primary']) ?>
                <a href="<?= Url::to(['index']) ?>" class="btn btn-default btn-outline-secondary"><?= Yii::t('app', 'reset') ?></a>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
