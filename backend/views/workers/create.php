<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Worker */

$this->title = Yii::t('app', 'createWorker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
