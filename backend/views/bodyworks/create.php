<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BodyWork */

$this->title = Yii::t('app', 'bodyworkCreate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'bodyworks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bodywork-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
