<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?=Html::encode($this->title)?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel, 'entities' => $entities]); ?>

    <?=GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'entityName',
            'content' => function ($model) {
                return Yii::t('app', $model->entityName);
            },
        ],
        [
            'attribute' => 'action',
            'content' => function ($model) {
                return Yii::t('app', $model->action);
            },
        ],
        'afterChange:ntext',
        'createdAt',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
        ],
    ],
]);?>


</div>
