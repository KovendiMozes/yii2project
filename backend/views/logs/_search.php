<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LogSearch */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="log-search search-box">

    <div class="row">
        <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
]);?>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'entityName',
    'staticValues' => $entities,
    'textTranslate' => true,
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\Select2::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'action',
    'staticValues' => [['id' => 'create', 'action' => 'create'], ['id' => 'update', 'action' => 'update']],
    'textTranslate' => true,
]);?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
            <?php echo $form->field($model, 'afterChange')->textInput(['placeholder' => $model->getAttributeLabel('afterChange') . ' (<ID>)', 'autocomplete' => 'off']) ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?=\common\widgets\DatePicker::widget([
    'form' => $form,
    'model' => $model,
    'attribute' => 'createdAt',
])?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <?=Html::submitButton(Yii::t('app', 'search'), ['class' => 'btn btn-primary'])?>
                <a href="<?=Url::to(['index'])?>" class="btn btn-default btn-outline-secondary"><?=Yii::t('app', 'reset')?></a>
            </div>
        </div>
        <?php ActiveForm::end();?>
    </div>



</div>
