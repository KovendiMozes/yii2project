<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
  
/* @var $this yii\web\View */
/* @var $model backend\models\Log */

$this->title = Yii::t('app', $model->entityName) . ' (' . $model->createdAt . ')';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="log-view">

    <h1><?=Html::encode($this->title)?></h1>

    <?=DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'entityName',
            'value' => function ($model) {
                return Yii::t('app', $model->entityName);
            },
        ],
        'beforeChange:ntext',
        'afterChange:ntext',
        [
            'attribute' => 'userId',
            'value' => function ($model) {
                return $model->user->name;
            },
        ],
        'ip',
        'createdAt',
    ],
])?>

</div>
