<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Gearbox */

$this->title = Yii::t('app', 'update'). ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'gearboxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'update');
?>
<div class="gearbox-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
