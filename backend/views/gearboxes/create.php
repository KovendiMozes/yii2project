<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Gearbox */

$this->title = Yii::t('app', 'createGearbox');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'gearboxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gearbox-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
