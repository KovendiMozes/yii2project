<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\EquipmentLevel */

$this->title = Yii::t('app', 'createEquipmentLevel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'equipmentLevels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-level-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
